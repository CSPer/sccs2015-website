<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Call for Abstracts";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>
<body>

    <?php include("includes/bodyTop.php"); ?>
    <?php include("includes/pageTitle.php"); ?>
    <?php include("includes/navigation.php"); ?>

    <div class="content">
        <div class="block">
            

            The 5th Student Conference on Complexity Science will take place between the 
            9th and 11th September 2015 in GRANADA, SPAIN, organised by the Institute for 
            Complex System Simulation, University of Southampton.<br><br>

            The conference brings together high quality, interdisciplinary research from 
            across the full spectrum of complexity science, and is run by PhD students for 
            PhD students and early career researchers. It represents a unique and 
            economical opportunity to meet, interact and network with a cross-section of 
            around 200 emerging complexity science researchers in a stimulating, supportive 
            and interdisciplinary environment.<br><br>

            The conference programme will comprise a mixture of research talks, poster 
            sessions, workshops and tutorials, plenary keynote lectures, and social events.<br><br>

            The SCCS programme committee invite abstract submissions on new and recently 
            published work from all areas of complexity science and complex systems 
            research. This includes, but is not limited to, topics such as:
             <br /><br/>
            
            <div class="abstract-list">
            <ul>
                <li><h3><span>Methodology</span></h3></li>
 					Mathematical and simulation modelling methodology, new tools and techniques<br /><br />
 				<li><h3><span>Network Science</span></h3></li>
 					Technological networks, spatial networks, infrastructure, ecology, social networks and the internet<br /><br />
 				<li><h3><span>Planning and Industry</span></h3></li>
 					Critical infrastructures, urban planning, mobility, transport, sustainability<br /><br />
				<li><h3><span>Earth System Complexity</span></h3></li>
					Climate change, ocean, atmosphere, ice and solid earth dynamics<br /><br />
 				<li><h3><span>Biological Complexity</span></h3></li>
 					Systems biology, ecology, ecosystem services, medicine, neuroscience<br /><br />
 				<li><h3><span>Evolution</span></h3></li>
 					Origins of life, major evolutionary transitions, generative and developmental systems<br /><br />
 				<li><h3><span>Artificial Intelligence</span></h3></li>
 					Swarm intelligence, embodied cognition, robotics, artificial life<br /><br />
 				<li><h3><span>Social Systems</h3></li>
 					 Linguistics, demography, psychology, health, past societies<br /><br />
 				<li><h3><span>Economics and Finance</span></h3></li>
 					Markets and stability, trade, public policy, game theory<br /><br />
 				<li><h3><span>Engineering and Physical Sciences</span></h3></li>
 					Quantum dynamics, statistical mechanics, optimisation, turbulence, computational chemistry, nanotechnology, energy<br /><br />
            <br /><br />
            </ul>
            </div>
            
            
                
            Early-career researchers and PhD students in particular are invited to submit
            an abstract for consideration for oral or poster presentation. Abstracts will be
            submitted through an online submission system, in plain text, and with a limit of 
            300 words. All abstracts will be peer reviewed for relevance and quality.
            <br /><br />
             
            <div class='abstract-subs'>
                KEY DATES
            </div> <br/> <br/>
             
           
                
                Deadline for submissions: 1st of June, 2015 <br/>
                Notification of acceptance: June 2015 <br/>
                Main Conference convenes:   9-11 September, 2015 <br/>
            <br/> 
                You can now submit an abstract by registering and logging in to the online submission system:
                <br/><br/>
                <a href="Login.php" class="actionButtonSubmit">
                    <span class="text-large">Registration</span>
                </a>
            
            <br/><br/><br/><br/>
             
            <div class='abstract-subs'>GRANADA</div>  <br/> <br/>
             
            
                
               The conference will take place in central Granada, a beautiful city
               in the foothills of the Sierra Nevada mountains in Southern Spain. 
               It is the site of the world-famous Alhambra palace, a 14th Century Moorish
               citadel. Along with the Generalife, one of the oldest surviving Moorish water
               gardens, and the winding streets of the Albayzin, the Alhambra is recognised as
               a UNESCO World Heritage Site (<a href="http://whc.unesco.org/en/list/314" target="_blank">http://whc.unesco.org/en/list/314</a>).
            <br/> <br/>
            
            Information about Hotels and Restaurants in Granada can be found in the Granada
            section (left side panel) or in the following link:

            
            <br/><br/>
                <a href="granada_info.php" class="actionButtonSubmit">
                    <span class="text-large">Granada Information</span>
                </a>
            <br><br/><br/>
            
            <div class="abstract-subs">PRACTICALITIES</div>  <br/> <br/>
             
            
                <table id="abstable">
                <tr>
                <td class="leftcol"><b>Costs:</b></td>
                <td class="rightcol">The registration fee for the conference will be low - around £50/€70.</td>
                </tr>
                
                <tr>
                <td class="leftcol"><b>Venue: </b></td>
                <td class="rightcol"><a href="http://www.pcgr.org/en/">Granada Congress and Exhibition Centre </a> </td>
                </tr>
                
                <tr>
                <td class="leftcol"><b>Accommodation: </b></td>
                <td class="rightcol">There are plenty of reasonably priced hotels and guest houses in close proximity</td>
                </tr>
                
                <tr>
                <td class="leftcol"><b>Travel: </b></td>
                <td class="rightcol">Granada has its own airport. Many reasonably priced international flights arrive at nearby Malaga airport. </td>
                </tr>
                
                <tr>
                <td class="leftcol"><b>Bursaries:</b> </td>
                <td class="rightcol">We hope to be able to offer travel bursaries to a number of delegates</td>
                </tr>
                
                </table>
                <br/> <br/>

                We look forward to seeing you in Granada!  <br/>
                For any enquiries please email: <a href="mailto:sccs15@soton.ac.uk">sccs15@soton.ac.uk</a>
                <br><br>

        </div>
    </div>
   
    <?php include("includes/bodyBottom.php"); ?>

</body>
</html>
