<?php

session_start();
if (isset($_SESSION['SignIn']) && $_SESSION['SignIn'] != '') {
    header ("Location: tabs_login.php");
}

include("functions/loginf.php"); // includes LogIn() function

$err = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if(isset($_POST['login'])) // call function SignIn() if the user clicks the signin button
	{
		$err=LogIn();
	}
	if(isset($_POST['signup'])) // if the user clicks the Sign Up button
	{
		header("Location: ./SignUp.php");
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Login";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>
<?php include("includes/pageTitle.php"); ?>
<?php include("includes/navigation.php"); ?>

<div class="content">
    <div class="block" style="font-size:14px;">
    To submit an abstract you need to Log in or create a new account. 
    The payment process for the conference will open and be accessible 
    through your acount once abstract submissions have been reviewed. 

    </div>
</div>

<br> <br>

<?php echo $err; ?>
<div id="Sign-In">
    <legend><font color="white"> LOG-IN HERE</font></legend><br>
    <form method="POST" action="Login.php">
    <font color="white">Email </font> <br><input type="text" name="email"><br> <br>
    <font color="white">Password </font> <br><input type="password" name="pass"><br> <br>
    <input id="button" type="submit" name="login" value="Log-In">
    <input id="button" type="submit" name="signup" value="Create Account"><br>
    </form>
    <br>
    <a href="password_reset.php" style="color: white; text-decoration:none;">Forgot your password?</a>
</div>

<!-- <?php include("includes/bodyBottom.php"); ?> -->

</body>
</html>
