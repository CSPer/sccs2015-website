<?php
// The following code uses a catcha generator from GITHUB
// https://github.com/claviska/simple-php-captcha
session_start();
if (!isset($_SESSION['Errors'])) {
    $_SESSION['Errors'] = array(    'errormessage'         => '',
                                    'firstnameErr'         => '',
                                    'lastnameErr'          => '',
                                    'emailErr'             => '',
                                    'pass1Err'             => '',
                                    'pass2Err'             => '',
                                    'captchaErr'           => '',
                                    'other_affiliationErr' => '',
                                    'other_positionErr'    => '',
                                    'other_sourceErr'      => ''
                                );
}

include("captcha/simple-php-captcha/simple-php-captcha.php");

// Options for the captcha generator
$_SESSION['captcha'] = simple_php_captcha( array(
    'min_length' => 6,
    'max_length' => 6,
    'characters' => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789',
    'min_font_size' => 28,
    'max_font_size' => 28,
    'color' => '#666',
    'angle_min' => 0,
    'angle_max' => 11,
    'shadow' => true,
    'shadow_color' => '#fff',
    'shadow_offset_x' => -1,
    'shadow_offset_y' => 1
));


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Welcome to SCCS 2015";
	$pageDescription = "The Student Conference on Complexity Science (SCCS) is the largest UK conference for early-career researchers working under the interdisciplinary framework of Complex Systems, with a particular focus on computational modelling, simulation and network analysis. Since 2010, this conference series has brought together PhD students and early career researchers from both the UK and overseas, whose interests span areas as diverse as quantum physics, ecological food webs or the economics of happiness. This interdisciplinary nature of the conference is reflected by the diversity of keynote speakers as well as practical, hands-on workshops";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<head>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jquery", "1.4.4");
    </script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#other_affiliation").hide();
            $('#affiliation').change(function() {
              if($(this).find('option:selected').val() == "Other"){
                $("#other_affiliation").show();
              }else{
                $("#other_affiliation").hide();
              }
            });
    });
    </script>

    <!-- Hidden box for the 'Position' entry -->
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#other_position").hide();
            $('#position').change(function() {
              if($(this).find('option:selected').val() == "Other_P"){
                $("#other_position").show();
              }else{
                $("#other_position").hide();
              }
            });
    });
    </script>


    <!-- Hidden box for the 'Source' entry -->
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#other_source").hide();
            $('#source').change(function() {
              if($(this).find('option:selected').val() == "Other"){
                $("#other_source").show();
              }else{
                $("#other_source").hide();
              }
            });
    });
    </script>

</head>

<body>
<?php include_once("analyticstracking.php") ?>

<?php include("includes/bodyTop.php"); ?>
<?php include("includes/pageTitle.php"); ?>


<?php include("includes/navigation.php"); ?>

<div id="Sign-Up">
    <legend><font color="white"><h2>Create Account</h2></font></legend><br>
	<p><span class="error">* required field</span></p> <br>

	<form method="POST" action="SignUpAfter.php">
		First Name <br><input type="text" name="firstname" size="20px"><?php echo $_SESSION['Errors']['firstnameErr'];?>
		<span class="error">* </span><br><br>

		Last Name <br><input type="text" name="lastname" size="20px"><?php echo $_SESSION['Errors']['lastnameErr'];?>
		<span class="error">* </span><br><br>

		Email <br><input type="text" name="email" size="20px"><?php echo $_SESSION['Errors']['emailErr'];?>
		<span class="error">* </span><br> <br>

        Institution:
        <fieldset id="affiliations">
            <select name="affiliation" id="affiliation">
                <option value="ICSS_DTC" /> University of Southampton ICSS DTC </option>
                <option value="University of Southampton" /> University of Southampton </option>
                <option value="Bristol DTC" /> Bristol DTC </option>
                <option value="Warwick DTC" /> Warwick DTC </option>
                <option value="ICSS Alumni" /> ICSS alumni </option>
                <option value="Other" /> Other affiliation </option>
            </select><?php echo $_SESSION['Errors']['other_affiliationErr'];?>
            <br>
            <input id="other_affiliation" name="other_affiliation" type="text" placeholder=""/>
            <span class="error">* </span><br>
            <br>
        </fieldset>

        Position:
        <fieldset id="position">
            <select name="position" id="position">
                <option value="PhD" /> PhD </option>
                <option value="MSc" /> MSc </option>
                <option value="Post_Doctorate" /> Post-Doctorate </option>
                <option value="Other_P" /> Other </option>
            </select><?php echo $_SESSION['Errors']['other_positionErr'];?>
            <br>
            <input id="other_position" name="other_position" type="text" placeholder=""/>
            <span class="error">* </span><br>
            <br>
        </fieldset>

        Password <br>
        <input type="password" name="pass1" size="20px"><?php echo $_SESSION['Errors']['pass1Err'];?><br>
        <span class="error">* </span><br> <br>

        Re-type Password <br>
        <input type="password" name="pass2" size="20px"><?php echo $_SESSION['Errors']['pass2Err'];?><br>
        <span class="error"><font color="red">* </font></span><br><br>

            <!-- Where did you find us? -->
         Where did you find us?
         <fieldset id="source">
            <select name="source" id="source">
                <option value="empty" /> --------Please Choose One--------</option>
                <option value="Google" /> Google</option>
                <option value="Twitter" /> Twitter </option>
                <option value="Facebook" /> Facebook </option>
                <option value="Poster" /> Poster</option>
                <option value="Email" /> Email</option>
                <option value="Friend" /> From a friend </option>
                <option value="Other" /> Other </option>
            </select><?php echo $_SESSION['Errors']['other_sourceErr'];?>
            <br>
            <input id="other_source" name="other_source" type="text" placeholder=""/>
            <span class="error">* </span><br>
            <br>
        </fieldset> 

            <!--    CAPTCHA    -->
            <br><br>
            <div style="text-align:center; display:block;">
            <?php
            // We put the Image here. The variable to compare
            // the submitted code is in $_SESSION['captcha']['code']
            echo "<img src=\"". $_SESSION['captcha']['image_src']."\"/>";
            ?>
            </div>
            <p style="font-size:14px;">Please type the captcha below<br>
            (reload the webpage if it is not visible)</p>
            <input type="text" name="captcha" size="17px"><?php echo $_SESSION['Errors']['captchaErr'];?>
            <br>
            <span class="error">* </span>
            <br><br>

            <input id="button" type="submit" name="submit" value="Submit">
	</form>
	<br>
	<?php echo $_SESSION['Errors']['errormessage'];?>
</div>


<?php include("includes/bodyBottom.php"); ?>
</body>
</html>
