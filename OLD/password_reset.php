<?php
$err = '';
include("functions/password_reset_f.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if(isset($_POST['password_reset'])) // call function SignIn() if the user clicks the signin button
	{
		$err=SendPassRes();
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Reset Password";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>
<?php include("includes/pageTitle.php"); ?>
<?php include("includes/navigation.php"); ?>


<div class="content">
    <div class="block" style="font-size:14px;">
    You can now input your email address and a link will be sent to your inbox 
    which, after clicking on it, your current password will be deleted and
    you will have to specify a new one.
    <br/><br/>
    </div>
</div>


<?php echo $err; ?>
<div id="Sign-In" style="margin-top:200px;">
    <legend><font color="white"> Input your email below:</font></legend>
    <form method="POST" action="password_reset.php">
    <br><input type="text" name="email"><br> <br>
    <input id="button" type="submit" name="password_reset" value="Confirm">
    </form>
</div>

<?php include("includes/bodyBottom.php"); ?>

</body>
</html>
