<?php

$pass1Err = "";
$pass2Err = "";
$key = $_GET['ID'];
$keyErr ="";

include("functions/password_reset_f.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if(isset($_POST['password_reset_confirm'])) // call function SignIn() if the user clicks the signin button
	{
        list($pass1Err, $pass2Err, $keyErr) = PassRes($key);
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Reset Password";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>



<div class="content">
    <div class="block" style="font-size:14px;">
    Please specify your new password below. If the password was changed successfully, you should see a green box
    with a message on top of the input box.
    <br/><br/>
    </div>
</div>


<?php echo $keyErr; ?>
<div id="Sign-In" style="margin-top:180px; color:white;">
    <form method="POST" action=<? echo "password_reset_confirmation.php?ID=" . $key ?>>

    New Password <br>
    <input type="password" name="pass1" size="20px"><?php echo $pass1Err;?><br>
    
    Re-type New Password <br>
    <input type="password" name="pass2" size="20px"><?php echo $pass2Err;?><br>
    <br/>
    <input id="button" type="submit" name="password_reset_confirm" value="Confirm">
    </form>
</div>

<?php include("includes/bodyBottom.php"); ?>

</body>
</html>
