<?php
session_start();
if (!(isset($_SESSION['SignIn']) && $_SESSION['SignIn'] != '')) {
    header ("Location: Login.php");
}


$_SESSION['abstractErr'] = '';
$_SESSION['abstractErr2'] = '';
$_SESSION['abstract_titleErr'] = '';
$_SESSION['presentationErr'] = '';
$_SESSION['keywordsErr'] = '';
$_SESSION['sourceErr'] = '';
$tab=1;

include("functions/submitAbstractf.php");
include("functions/submitInfof.php");
include("functions/Paymentf.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  // Call function addPerson() if the user clicks the submit button
    if(isset($_POST['submit']))
    {
        $tab=2;
        SubmitAbstract();
    //list($errorMessage, $firstnameErr,$lastnameErr, $emailErr, $pass1Err, $pass2Err, $captchaErr) = addPerson();
    }
    
    // Submit Information
    if(isset($_POST['submit_info']))
    {
        $tab=1;
        SubmitInfo();
    //list($errorMessage, $firstnameErr,$lastnameErr, $emailErr, $pass1Err, $pass2Err, $captchaErr) = addPerson();
    }

    if(isset($_POST['payment']))
    {
        $tab=3;
        Payment();
    }
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Registration Form";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<head>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
        google.load("jquery", "1.4.4");
</script>


<!-- Hidden box for the 'Source' entry -->
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#other_source").hide();
            source_val = "<?php echo $_SESSION['source'];?>";
            if (!(source_val === "Google" || source_val === "Twitter" || source_val === "Facebook" || source_val === "Poster" || source_val === "Email" || source_val === "Friend")){
                $('[name=source]').append($('<option>', {value: source_val, text: source_val}));
            }
            $('[name=source]').val(source_val);
            $('#source').change(function() {
              if($(this).find('option:selected').val() == "Other"){
                $("#other_source").show();
              }else{
                $("#other_source").hide();
              }
            });

            $("#DietReq").hide();
            diet_val = "<?php echo $_SESSION['diet'];?>";
            if (diet_val === "yes"){
                $("#DietReq").show();
            }
            $('[name=Diet]').val(diet_val);
            $("#Diet").change(function() {
                if($(this).find('option:selected').val() == "yes"){
                    $("#DietReq").show();
                }else{
                    $("#DietReq").hide();
                }
            });
        });
    </script>
</head>

<body>
    <?php include("includes/bodyTop.php"); ?>
    <?php include("includes/pageTitle.php"); ?>
    <?php include("includes/navigation.php"); ?>

<div class="content">

    <!-- ERRORS will appear at the top of the page -->
    <?php echo $_SESSION['abstractErr2'];?>
    <?php echo $_SESSION['abstract_titleErr'];?>
    <?php echo $_SESSION['abstractErr'];?>
    <?php echo $_SESSION['presentationErr'];?>
    <?php echo $_SESSION['keywordsErr'];?>
    <?php echo $_SESSION['sourceErr'];?>

    <!-- Information for ICSS DTC students -->
    <?php echo $_SESSION['affiliation_information'];?>
    <?php echo $_SESSION['payment_information'];?>
    

    <div class="instructions">
        <div style="color:#005C84; font-weight: bold; font-size: 18pt;">INSTRUCTIONS</div><br/>
        <div class="instructions-list">
            <ul>
            <!--
            <li>
                You can update your first and last name by editing the corresponding 
                text fields and clicking the Submit Information button in the
                Personal Information tab.
            </li>
            <li>
                In the Abstract Tab, you can update your abstract information 
                and your preferred way of presenting anytime until 
                the Abstract Submission deadline (<b>1st June 2015</b>). A pop-up notification
                should appear once you press the submit button, otherwise you can check
                your submission going back to the Abstract tab or from the green box message
                at the top of the page.
            </li>
            <li>
                Optionally, you can add up to 4 keywords related to your abstract before submission..
            </li>
            -->
            <a id="form"></a>
            <li>

                In order to access the payment system, please fill out the form under the <a href="tabs_login.php?tab=true#form">registration tab </a> according to your needs.              
            </li>
            <li>
                After submission of the form you will be able to access the payment system using the validation code, please make note of this. Follow the link
                to be directed to the University of Southampton payment system. Please accept our apologies, as the university
                handles the payment you will be required to fill in your details again.
            </li>
            <li>
                In the University of Southampton registration process, we recommend you to use the same email as this account.
            </li>
            <li>
                If you have any queries please do not hesitate to get in touch with the committee at <a href="mailto:sccs2015@soton.ac.uk">sccs2015@soton.ac.uk</a>
            </li>
        
            </ul>
        </div>
    </div>
    <?php 
        if (isset($_GET['tab'])) {
            $tab=3;
        }
    ?>
 
    <ul class="tabs">
        <li>
            <input type="radio" name="tabs" id="tab1" <?php if ($tab == 1 ) echo 'checked'; else {echo '';} ?> >
            <label for="tab1">Personal Information</label>
            <div id="tab-content1" class="tab-content">
             <div class="container">
                 <form action="tabs_login.php#form" method="POST">
                    <div class="text_color">
                        First Name: <input type="text" name="firstname" value = "<?php echo $_SESSION['firstname'];?>" ><br>
                        <br>
                        Last Name: <input type="text" name="lastname" value = "<?php echo $_SESSION['lastname'];?>" ><br>
                        <br>
                        E-mail: <input type="text" name="email" value= "<?php echo $_SESSION['email'];?>" readonly><br>
                        <br>
                        Institution: <input type="text" name="affiliation" value= "<?php echo $_SESSION['affiliation'];?>" readonly><br>
                        <br>
                        Position: <input type="text" name="position" value= "<?php echo $_SESSION['position'];?>" readonly> <br>
                        <br>

                        Where did you find us?
                        <fieldset id="source">
                            <select name="source" id="source">
                            <option value="Google" /> Google </option>
                            <option value="Twitter" /> Twitter </option>
                            <option value="Facebook" /> Facebook </option>
                            <option value="Poster" /> Poster</option>
                            <option value="Email" /> Email</option>
                            <option value="Friend" /> From a friend </option>
                            <option value="Other" /> Other </option>
                            </select><br>
                            <input id="other_source" name="other_source" type="text" placeholder=""/>
                            <br>
                        </fieldset> 
                    </div>
                    <br>
                    <input type="submit" name="submit_info" value="Submit Information">
                    </form>
                </div>
            </div>
        </li>

        <li>
            <input type="radio" name="tabs" id="tab2" <?php if ($tab == 2 ) {echo 'checked';} else {echo '';} ?>  >
            <label for="tab2" style="text-align: center; padding-top:10px;">Abstract <br> Submission</label>

            <div id="tab-content2" class="tab-content">
                <form action="tabs_login.php#form" method="POST">
                    <h3>Abstract Title:</h3> <br>
                    <div class="textwrapper"><input type="text" name="abstract_title" value= "<?php echo $_SESSION['abstract_title'];?>" size="100%" readonly></div>
                    <!--<span class="error" style="font-weight:bold;"> <?php echo $_SESSION['abstract_titleErr'];?></span><br>-->
                    <br>
                    
                    In the next box, write your abstract, which must be no longer than 350 words.<br>
                    
                    <!-- The echo is edited in a way that the breaklines appear as such, and not as \r\n. We also remove the ' characters -->
                    <div class="textwrapper" readonly><textarea name="abstract" rows="20" cols="20" readonly /><?php echo  $abstract = str_replace( "\\r\\n", "\r\n", $_SESSION['abstract']);  ?> </textarea></div>
                    <!-- <span class="error" style="font-weight:bold;"> <?php echo $_SESSION['abstractErr'];?></span><br><br> -->

                    <span> Choose your preferred way of presentation: </span>
                    <select name="presentation">
                        <option value="None"  <?php if ($_SESSION['presentation'] == "None" ) echo ' selected="selected"'; ?>     /> None </option>
                        <option value="Poster"  <?php if ( $_SESSION['presentation'] == "Poster" ) echo ' selected="selected"'; ?> /> Poster</option>
                        <option value="Oral" <?php if ( $_SESSION['presentation'] == "Oral" ) echo ' selected="selected"'; ?>      /> Oral </option>
                        <option value="Both" <?php if ($_SESSION['presentation'] == "Both" ) echo ' selected="selected"'; ?>      /> Both </option>
                    </select>
                    <br><br>
                    <h3>Keywords:</h3> 
                    <div class="textwrapper" readonly><input readonly type="text" name="keywords" value= "<?php echo $_SESSION['keywords'];?>" size="100%"></div>
                    <br><br>
                    <!-- <input type="submit" name="submit" value="Submit Abstract"> -->
                </form>
            </div>
        </li>
        <li>
            <input type="radio"  name="tabs" id="tab3" <?php if ($tab == 3 ) echo 'checked'; else {echo '';} ?>>
            <label for="tab3">Registration</label>
            <div id="tab-content3" class="tab-content">
                <form action="tabs_login.php" method="POST">
                    Do you have any dietary requirements?
                        <fieldset id="Diet">
                            <select name="Diet" id="Diet">
                            <option value="no" /> no </option>
                            <option value="yes" /> yes </option>
                            </select><br>
                            <input id="DietReq" name="DietReq" type="text" value= "<?php echo $_SESSION['dietReq'];?>" placeholder=""/>
                            <br>
                        </fieldset>
                    Do you require any special assistance? <br>
                        <select name="Assistance" id="Assistance">
                            <option value="no"  <?php if ($_SESSION['assistance'] == "no" )  echo ' selected="selected"'; ?> /> no </option>
                            <option value="yes" <?php if ($_SESSION['assistance'] == "yes" ) echo ' selected="selected"'; ?> /> yes </option>
                        </select><br><br>
                    Would you be interested in joining a trip to the <a href="http://www.alhambradegranada.org/en/" style="color: white"> <b> Alhambra Palace</b> </a> on thursday evening?<br> (10th of September)<br>
                        <select name="Trip" id="Trip">
                            <option value="no"  <?php if ($_SESSION['trip'] == "no" )  echo ' selected="selected"'; ?> /> no </option>
                            <option value="yes" <?php if ($_SESSION['trip'] == "yes" ) echo ' selected="selected"'; ?> /> yes </option>
                        </select><br><br>


                    <input type="submit" name="payment" value="Submit Information">
                </form><br><br>
                
                
            </div>
        </li>

        <li>
            <!--<input type="radio" name="tabs" id="tab3" /> -->
            <label for="tab4"><a href=logout.php style="color: white;" > LogOut </a></label>
        </li>

    </ul>

    <br style="clear: both;" />

</div>
</body>
