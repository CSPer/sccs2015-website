<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Contact";
	//$pageDescription = "";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>
<?php include_once("analyticstracking.php") ?>
<?php include("includes/bodyTop.php"); ?>
		<?php include("includes/pageTitle.php"); ?>
    	
        
        <?php include("includes/navigation.php"); ?>
        
        <div class="content">
            If you have any questions, please do not hesitate to contact us: <a href="mailto:sccs15@soton.ac.uk">sccs15@soton.ac.uk</a><br /><br />

<strong>Stay up-to-date! Follow us on <a href="http://twitter.com/sccs2015" target="_blank">Twitter</a> and join the <a href="https://www.facebook.com/sccs2015" target="_blank">Facebook</a> group.</strong>

        </div>
        
<?php include("includes/bodyBottom.php"); ?>
   


</body>
</html>
