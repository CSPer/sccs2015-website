bplist00�_WebMainResource�	
_WebResourceFrameName^WebResourceURL_WebResourceData_WebResourceMIMEType_WebResourceTextEncodingNameP_6https://dl.dropboxusercontent.com/u/44711370/ToyFDM.pyOB�<html><head><style type="text/css"></style></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">""" Martin Wood - University of Southampton
	21/07/2014
	
	Toy model - 1D advection, for SCCS2014 workshop on finite difference
	models. """

"""Import modules"""
import numpy                           #  Has arrays and matrices
from time import sleep                 #  This function just pauses the execution for a bit
import pygame                          #  Meant to animate computer games, good for realtime graphics output.
from Tkinter import *                  #  Functions for building the interface (GUI)

""" Model parameters """
N = 200                     # Number of elements/ mesh points for 1D model
TimeStep = 0.1             # Size of each iterative step forward in time (s)
LeftBoundary = 1.0         # Concentration at the left ("upwind") end of the model
RightBoundary = 0.0        # Concentration at the right ("downwind") end of the model
Length = 1.0               # Length of the modelled concentration profile (m)
FlowRate = 0.01           # Rate of advection (m/s)
Time = 200                 # Duration for which to run the model (s) (model time, not REAL time!)

""" Define the colours """
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREY     = (  50,  50,  50)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
BLUE     = (   0,   0, 255)

WIDTH = 700                # Width of display in pixels
HEIGHT = 200               # Height of display in pixels



""" Create an application window &amp; interface, etc """
class App:
	
	def __init__(self, master):
		
		""" Initiation stuff sets up the window, scroll bars, etc """
		self.canvas = Canvas(master, borderwidth=0, bg="LightGreen")
		frame = Frame(self.canvas, bg="LightGreen")
		frame.pack()
		self.vsb = Scrollbar(master, orient="vertical", command=self.canvas.yview, bg="LightGreen")
		self.hsb = Scrollbar(master, orient="horizontal", command=self.canvas.xview, bg="LightGreen")
		self.canvas.configure(yscrollcommand=self.vsb.set, xscrollcommand=self.hsb.set, bg="LightGreen")
		
		self.vsb.pack(side="right", fill="y")
		self.hsb.pack(side="bottom", fill="x")
		self.canvas.pack(side="left", fill="both", expand=True)
		self.canvas.create_window((4,4), window=frame, tags="frame")
		
		frame.bind("&lt;Configure&gt;", self.OnFrameConfigure)

		""" Variables to hold model parameters """
		self.N = IntVar()
		self.BoundaryType = IntVar()
		self.ModelStep = IntVar()
		self.initial = IntVar()
		self.TimeStep = DoubleVar()
		self.LeftBoundary = DoubleVar()
		self.RightBoundary = DoubleVar()
		self.Length = DoubleVar()
		self.FlowRate = DoubleVar()
		self.Time = DoubleVar()
		self.CFL = DoubleVar()

		""" Labels for boxes for the various variables """
		Label(frame, text="--- Parameters ---\n   ", bg="LightGreen").grid(row=1, column=0)
		Label(frame, text="--- Values ---\n   ", bg="LightGreen").grid(row=1, column=1)
		Label(frame, text=" ", bg="LightGreen").grid(row=2, column=0)
		Label(frame, text=" ", bg="LightGreen").grid(row=2, column=1)
		Label(frame, text=" ", bg="LightGreen").grid(row=2, column=2)
#		Label(frame, text="Left (upwind) boundary value", bg="LightGreen").grid(row=5, column=0)
#		Label(frame, text="Right (downwind) boundary value", bg="LightGreen").grid(row=6, column=0)
		Label(frame, text="Length of modelled section (m)", bg="LightGreen").grid(row=7, column=0)
		Label(frame, text="Rate of flow (m/s)", bg="LightGreen").grid(row=8, column=0)
		Label(frame, text="Total time to run (s)", bg="LightGreen").grid(row=9, column=0)

		""" And these are the actual entry boxes, linked to the variables above """
#		Entry(frame, textvariable=self.LeftBoundary).grid(row=5, column=1)
#		Entry(frame, textvariable=self.RightBoundary).grid(row=6, column=1)
		Entry(frame, textvariable=self.Length).grid(row=7, column=1)
		Entry(frame, textvariable=self.FlowRate).grid(row=8, column=1)
		Entry(frame, textvariable=self.Time).grid(row=9, column=1)
		
		Label(frame, text=" ", bg="LightGreen").grid(row=10, column=0)
		
		Label(frame, text="Gradient Approximation", bg="LightGreen").grid(row=11, column=0)
		self.ModelStepBox = Spinbox(frame, values=("Backward", "Forward", "Central"))
		self.ModelStepBox.grid(row=11, column=1)
		
		Label(frame, text="Boundary Condition", bg="LightGreen").grid(row=12, column=0)
		self.BoundaryTypeBox = Spinbox(frame, values=("Dirichlet", "Neumann", "Mixed"))
		self.BoundaryTypeBox.grid(row=12, column=1)
		
		Label(frame, text="Initial Condition", bg="LightGreen").grid(row=13, column=0)
		self.initialBox = Spinbox(frame, values=("Step Function", "Wave Form"))
		self.initialBox.grid(row=13, column=1)
		
		Label(frame, text=" ", bg="LightGreen").grid(row=14, column=0)
		
		Label(frame, text="Number of mesh points", bg="LightGreen").grid(row=15, column=0)
		Scale(frame, from_=20, to=600, orient=HORIZONTAL, variable=self.N, bg="LightGreen").grid(row=15, column=1)
		Label(frame, text="Timestep (milliseconds)", bg="LightGreen").grid(row=16, column=0)
		Scale(frame, from_=1, to=100, orient=HORIZONTAL, variable=self.TimeStep, bg="LightGreen").grid(row=16, column=1)
		
		Label(frame, text=" ", bg="LightGreen").grid(row=17, column=0)
		
		""" CFL calculator """
		Button(frame, text="Calculate CFL", command=self.calculateCFL).grid(row=18, column=0)
		Entry(frame, textvariable=self.CFL).grid(row=18, column=1)
		
		Label(frame, text=" ", bg="LightGreen").grid(row=19, column=0)
		
		Button(frame, text="Run Model", command=self.run_everything).grid(row=20, column=1)

		""" Set the default values of the variables """
		self.N.set(100)
		self.BoundaryType.set(0)
		self.ModelStep.set(0)
		self.initial.set(0)
		self.TimeStep.set(20.0)
		self.LeftBoundary.set(1.0)
		self.RightBoundary.set(0.0)
		self.Length.set(1.0)
		self.FlowRate.set(0.05)
		self.Time.set(10.0)
		self.calculateCFL()
		
	# Something to do with configuring the scaleable canvas and scroll bar
	# I do NOT understand this part
	def OnFrameConfigure(self, event):
		# Reset the scroll region to encompass the inner frame
		self.canvas.configure(scrollregion=self.canvas.bbox("all"))
		
		return "Window embedded in canvas, not sure why"

		
	def calculateCFL(self):
		self.CFL.set( ( self.TimeStep.get()/1000.0 * self.FlowRate.get() ) / ( self.Length.get() / self.N.get() ) )
		return 0
			
	def run_everything(self):
		if ( self.ModelStepBox.get() == "Backward" ):
			self.ModelStep.set(0)
		elif ( self.ModelStepBox.get() == "Forward" ):
			self.ModelStep.set(1)
		elif ( self.ModelStepBox.get() == "Central" ):
			self.ModelStep.set(2)
		
		if ( self.BoundaryTypeBox.get() == "Dirichlet" ):
			self.BoundaryType.set(0)
		elif ( self.BoundaryTypeBox.get() == "Neumann" ):
			self.BoundaryType.set(1)
		elif ( self.BoundaryTypeBox.get() == "Mixed" ):
			self.BoundaryType.set(2)
		
		if ( self.initialBox.get() == "Step Function" ):
			self.initial.set(0)
		elif ( self.initialBox.get() == "Wave Form" ):
			self.initial.set(1)
		
		playGame(self.N.get(), (self.TimeStep.get() / 1000.0), self.LeftBoundary.get(), self.RightBoundary.get(), self.Length.get(), self.FlowRate.get(), self.Time.get(), self.BoundaryType.get(), self.ModelStep.get(), self.initial.get())
		return 0

		
""" This is the code for the model itself.  There are two options here,
if you run the section of code labelled A, the model runs using a for loop
that makes the mathematics and actions of the model clear.  If you run using
section B, the model uses iteration over arrays to do the same mathematics
far more rapidly but much less transparently. """
def Model(CurrentConcentration, screen, N, TimeStep, LeftBoundary, RightBoundary, Length, FlowRate, Time, BoundaryType, ModelStep, initial):
	
	""" Calculate the spacing between points. """
	Spacing = Length / N
	
	""" Loop until the user clicks the close button """
	done = False
	
	""" Used to manage how fast the screen updates """
	clock = pygame.time.Clock()
	
	""" Used to store the results turn by turn. """
	Results = numpy.empty([N+10, int(Time / TimeStep)+10])
	
	""" Set the initial conditions. """
	if initial == 1:
		CurrentConcentration = setInitialWave(N, CurrentConcentration)
	
	t = 0.0
	while not done:
		
		# Main event loop
		for event in pygame.event.get():       # User did something
			if event.type == pygame.QUIT:      # If user clicked close
				done = True                    # Flag that we are done so we exit this loop
		if t &gt; Time:
			done = True
 
		# --- Game logic should go here			
		""" Set the model boundary """
		if initial == 0:
			if BoundaryType == 0:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryDirichlet(1.0, 0.0)
			elif BoundaryType == 1:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryNeumann(N, CurrentConcentration, t, 1.0, 0.0)
			elif BoundaryType == 2:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryMixed(N, CurrentConcentration, Spacing, t, 1.0, 0.0)
		else:
			if BoundaryType == 0:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryDirichlet(0.0, 0.0)
			elif BoundaryType == 1:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryNeumann(N, CurrentConcentration, t, 0.0, 0.0)
			elif BoundaryType == 2:
				CurrentConcentration[0], CurrentConcentration[N] = setBoundaryMixed(N, CurrentConcentration, Spacing, t, 0.0, 0.0)
		
		""" Calculate the change in concentration at each point. """
		if ModelStep == 0:
			parm = ModelStepBackward(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate)
		elif ModelStep == 1:
			parm = ModelStepForward(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate)
		elif ModelStep == 2:
			parm = ModelStepCentral(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate)
		else:
			parm = ModelStepBackward(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate)
		
		""" Update the model. """
		for x in xrange(1, N):
			CurrentConcentration[x] -= parm[x]
		
		# --- Drawing code should go here
		screen.fill(WHITE)
		
		pygame.draw.line(screen, GREY, [ 0, 0 ], [ WIDTH, 0 ])
		pygame.draw.line(screen, GREY, [ 0, int(HEIGHT/2) ], [ WIDTH, int(HEIGHT/2) ])
		pygame.draw.line(screen, GREY, [ 0, HEIGHT ], [ WIDTH, HEIGHT ])
		
			
		for x in xrange(0, N):
			
			""" Save the results for this turn """
			#Results[N, int(t/TimeStep)] = 0
			Results[x, int(t/TimeStep)] = CurrentConcentration[x]
			
			""" set the colour of marked mesh points according to the forward gradient. """

			if(CurrentConcentration[x] &gt; 0 ):
				colour = ( 255,   0,   0 )
			else:
				colour = (   0,   0, 255 )

			pygame.draw.circle(screen, colour, [ int((x*Spacing) * ( WIDTH / Length)) , int(HEIGHT - (CurrentConcentration[x] * (HEIGHT / LeftBoundary))) ], 6)
			
#			pygame.draw.circle(screen, colour, [ int((x*Spacing) * ( WIDTH / Length)) , int((HEIGHT * 1.5 * (t/Time)) + HEIGHT) ], 2)
		
		""" Draw the results so far. """
		for each in xrange(0, int(t/TimeStep), int(Time / TimeStep / 70)):
			for x in xrange(0, N, int(N/70) + 1):
				val = int(abs(Results[x, each]*255))
				if(val &gt;= 0 and val &lt; 256):
					colour = ( val,   0, 255 - val )
				elif(val == NaN):
					colour = (   0,   0, 255 )
				else:
					colour = (   0, 255,   0 )
				
				try:
					pygame.draw.circle(screen, colour, [ int((x*Spacing) * ( WIDTH / Length)) , int((HEIGHT * 1.5 * (each / (Time/TimeStep))) + HEIGHT*1.1) ], 6)
				except TypeError:
					print val
		
		t += TimeStep
 
		""" update screen """
		pygame.display.flip()
 
		""" Limit to 60 frames per second """
		clock.tick(60)

	return 0


""" Calculates and returns the model concentration profile changes for the next
turn, using the first order backward difference approximation. """
def ModelStepForward(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate):
	
	ChangeConcentration = []
	ChangeConcentration.append(0.0)    # Because the upper (left) boundary will never change
	
	""" Find the change in concentration for every mesh point """
	for x in xrange(1, N):                                                                      
		
		""" Find the concentration gradient around the mesh point x (equation REF) """
		Gradient = (CurrentConcentration[x+1] - CurrentConcentration[x]) / Spacing
		
		""" Calculate change in concentration using the gradient, rate of advection and the timestep (equation REF)"""
		ChangeConcentration.append( Gradient * FlowRate * TimeStep )
		
	return ChangeConcentration


""" Calculates and returns the model concentration profile changes for the next
turn, using the first order backward difference approximation. """
def ModelStepBackward(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate):
	
	ChangeConcentration = []
	ChangeConcentration.append(0.0)    # Because the upper (left) boundary will never change
	
	""" Find the change in concentration for every mesh point """
	for x in xrange(1, N):                                                                      
		
		""" Find the concentration gradient around the mesh point x (equation REF) """
		Gradient = (CurrentConcentration[x] - CurrentConcentration[x-1]) / Spacing
		
		""" Calculate change in concentration using the gradient, rate of advection and the timestep (equation REF)"""
		ChangeConcentration.append( Gradient * FlowRate * TimeStep )
		
	return ChangeConcentration


""" Calculates and returns the model concentration profile changes for the next
turn, using the second order central difference approximation. """
def ModelStepCentral(CurrentConcentration, Spacing, N, TimeStep, LeftBoundary, RightBoundary, FlowRate):
	
	ChangeConcentration = []
	ChangeConcentration.append(0.0)    # Because the upper (left) boundary will never change
	
	""" Find the change in concentration for every mesh point """
	for x in xrange(1, N):                                                                      
		
		""" Find the concentration gradient around the mesh point x (equation REF) """
		Gradient = ( (CurrentConcentration[x+1] - CurrentConcentration[x-1]) ) / ( 2 * Spacing )
		
		""" Calculate change in concentration using the gradient, rate of advection and the timestep (equation REF)"""
		ChangeConcentration.append( Gradient * FlowRate * TimeStep )
		
	return ChangeConcentration


""" Set the boundary of the model, using Dirichlet. In a Dirichlet type
boundary condition the boundary point is some function, in this case
f(x[0]) = 1, f(x[N]) = 0. """
def setBoundaryDirichlet(Left, Right):
	return Left, Right


""" Set the boundary of the model, using Neumann.  In this type of boundary
the gradient with displacement is set to zero.  In practice on a simple
model such as this, that means the boundary point value equals the value
of the next point in the grid. """
def setBoundaryNeumann(N, CurrentConcentration, t, Left, Right):
	if(t == 0):
		return Left, Right
	else:
		return CurrentConcentration[1], CurrentConcentration[N-1]


""" Set the boundary using a mixed scheme.  In this case, we set the boundary
using the gradient between the boundary point and the next innermost. """
def setBoundaryMixed(N, CurrentConcentration, Spacing, t, Left, Right):

	if(t == 0):
		return Left, Right
		
	""" Set boundary at beginning using forward difference first order approximation """
	grad = (CurrentConcentration[1] - CurrentConcentration[0]) / Spacing
	boundaryZero = CurrentConcentration[0] - (grad * TimeStep)
	
	""" Set boundary at end using backward difference first order approximation """
	grad = (CurrentConcentration[N] - CurrentConcentration[N-1]) / Spacing
	boundaryN = CurrentConcentration[N] - (grad * TimeStep)
	
	return boundaryZero, boundaryN


""" Resets the concentration profile to contain a wave form, to demonstrate advection
on something less susceptible to numerical diffusion and dispersion """
def setInitialWave(N, CurrentConcentration):
	space = int(N/4)
	temp = CurrentConcentration
	for x in xrange(0, space):
		temp[x] = 0.5 + ( numpy.sin( (numpy.pi * 1.5) + ( 2 * numpy.pi * (float(x)/space) ) ) / 2)
	for x in xrange(space, N):
		temp[x] = 0.0
	return temp
	
	
""" Initiate pygame for display, and activate the model """
def playGame(N, TimeStep, LeftBoundary, RightBoundary, Length, FlowRate, Time, BoundaryType, ModelStep, initial):
	
	pygame.init()
	
	""" Set the display width and height respectively """
	size = (WIDTH, HEIGHT*3)
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption("The Advection Model Visualised")
	
	""" Create an array to hold the concentration data """
	CurrentConcentration = numpy.array([0.0 for i in range(N+1)])
	
	"""-------- Main Program Loop ----------- """
	Model(CurrentConcentration, screen, N, TimeStep, LeftBoundary, RightBoundary, Length, FlowRate, Time, BoundaryType, ModelStep, initial)
		
	return 0
	

root = Tk()
root.wm_title("Tracer Transport Model")
root.geometry("430x450")
app = App(root)
root.configure(background='LightGreen')
root.mainloop()
</pre></body></html>]text/x-pythonUUTF-8    ( ? N ` v � � �CnC|                           C�