<?

function Payment(){
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php");
    $err = FALSE;
    $diet = test_input($_POST['Diet']);
    $dietReq = test_input($_POST['DietReq']);
    $assistance = test_input($_POST['Assistance']);
    $trip = test_input($_POST['Trip']);
    
    // Lengths to avoid information overflows
    $dietReqLen = strlen($dietReq);
    //$affiliationLen = strlen($affiliation);
    
    if ($dietReqLen > 50){
        echo "Diet requirement must be shorter than 50 characters";
    }

    if (!$err){
        $email = $_SESSION['email'];

        include('ConnectDB.php');

        $diet = sql_friendly($diet, $con);
        $dietReq = sql_friendly($dietReq, $con);
        $assistance = sql_friendly($assistance, $con);
        $trip = sql_friendly($trip, $con);
        $email = sql_friendly($email, $con);

        $query = "UPDATE SUBSCRIBERS SET Diet=$diet, DietReq=$dietReq, Assistance=$assistance, Trip=$trip WHERE Email=$email";
        
        // If the information was successfully recorded, send the
        // information back to show it in the Information tab page, using
        // the current SESSION variable
        if ($con->query($query) === TRUE) { 
            $_SESSION['diet'] = test_output($diet);
            $_SESSION['dietReq'] = test_output($dietReq);
            $_SESSION['assistance'] = test_output($assistance);
            $_SESSION['trip'] = test_output($trip);
            if ($_SESSION['affiliation'] !== "ICSS_DTC"){
                    $_SESSION['payment_information'] = "<a href=\"http://go.soton.ac.uk/6kq\" 
                        target=\"_blank\" class=\"payment-nonicss\"> Click here to go to 
                        the Payment Webpage. <br> Please use this <p>password</p> for the payment: <p>654as6d54</p> </a>";
            }

        } else {
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: submitinfof, line 46:  " . $query . "<br>" . $con->error);
            // echo "Error: " . $query . "<br>" . $con->error;
        }

        $con->close();
    }
}

?>
