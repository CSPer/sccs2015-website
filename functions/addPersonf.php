<?
function addPerson()
{
	//=====================================================FUNCTION addPerson() ===========================================================
	//	1) READS IN THE INPUTS FROM THE SIGNUP FORM AND APPLIES THE TEST_INPUT FUNCTION
	//	2) CHECKS IF THE INPUT VALUES HAVE THE REQUIRED INFORMATION
	//		2-A) IF NO INFORMATION IS GIVEN FOR THE REQUIRED FIELDS AN ERROR IS THROWN
	//		2-B) THE firstname AND lastname ENTRIES CAN ONLY BE LESS THAN 20 CHARACTERS AND CAN ONLY HAVE LETTERS AND WHITESPACE
	//		2-C) THE email ENTRY MUST BE IN EMAIL FORMAT AND BE LESS THEN 30 CHARACTERS
	//		2-D) THE TWO password ENTRIES MUST BE IDENTICAL (IN VALUE AND TYPE) AND BE LESS THAN 20 CHARACTERS
    //      2-E) Check if the captcha has been written correctly
	//  3) CHECKS IF THE GIVEN EMAIL HAS ALREADY BEEN REGISTERED IN THE DATABASE TABLE
	//  4) SAVES THE NEW ENTRIES TO THE DATABASE TABLE
	//	5) CREATES A SESSION FOR THE NEWLY REGISTERED USER AND REDIRECTS HIM/HER TO HIS/HER HOMEPAGE
	//====================================================================================================================================
	include("functions/safety.php"); // includes test_input() and sql_friendly() functions
	include("functions/error_write.php"); // Errors log file
	// INITIALIZATION OF THE ERROR MESSAGES
	$err = FALSE;
	$errorMessage = $firstnameErr = $lastnameErr = $emailErr = $pass1Err = $pass2Err = $captchaErr= $other_affiliationErr= $other_positionErr ='';

	//===========================================
	// CHECKS THE INPUT FOR UN-SUITABLE ENTRIES
	//===========================================
		// GET THE USER INPUTS
	$firstname = test_input($_POST['firstname']);
	$lastname = test_input($_POST['lastname']);
	$email = test_input($_POST['email']);
	$pass1 = test_input($_POST['pass1']);
	$pass2 = test_input($_POST['pass2']);
	$affiliation = test_input($_POST['affiliation']);
	$other_affiliation = test_input($_POST['other_affiliation']);
    $position = test_input($_POST['position']);
	$other_position = test_input($_POST['other_position']);
	$source = test_input($_POST['source']);
	$other_source = test_input($_POST['other_source']);
    $captcha = test_input($_POST['captcha']);
		// CALCULATE THE NUMBER OF CHARACTERS
	$fnameLength = strlen($firstname);
	$lnameLength = strlen($lastname);
	$emailLength = strlen($email);
	$passLength  = strlen($pass1);
	$other_affiliationLength  = strlen($other_affiliation);
    $other_positionLength  = strlen($other_position);
    $other_sourceLength = strlen($other_source);
		
    // DO THE SUITABILITY CHECKS
	// FIRSTNAME ----------------------
	if (empty($firstname)){
		$firstnameErr = "<span class=\"form_error\">First Name is required</span>";
		$err = TRUE;
	}
	else {
		if ($fnameLength > 20){
			$firstnameErr = "<span class=\"form_error\">First name must be less than 20 characters long</span>";
			$err = TRUE;
		}
	}
		// LASTNAME ---------------------
	if (empty($lastname)){
		$lastnameErr = "<span class=\"form_error\">Last name is required</span>";
		$err = TRUE;
	}
	else {
		if ($lnameLength > 20){
			$lastnameErr = "<span class=\"form_error\">Last name must be less than 20 characters long</span>";
			$err = TRUE;
		}
	}
		// EMAIL ------------------------
	if (empty($email)){
		$emailErr = "<span class=\"form_error\">Email is required</span>";
		$err = TRUE;
	}
	else {
		if ($emailLength > 40){
			$emailErr = "<span class=\"form_error\">Email must be less than 40 characters long</span>" ;
			$err = TRUE;
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	      	$emailErr = "<span class=\"form_error\">Invalid email format</span>";
	      	$err = TRUE;
	    }
	}

		// PASS1 -------------------------
	if (empty($pass1)){
		$pass1Err = "<span class=\"form_error\">Password is required</span>";
		$err = TRUE;
	}
	else {
		if ($passLength > 20){
			$pass1Err = "<span class=\"form_error\">Password must be less than 20 characters long</span>";
			$err = TRUE;
		}
	}
		// PASS2 -------------------------------
	if (empty($pass2)){
		$pass2Err = "<span class=\"form_error\">Re-type password is required</span>";
		$err = TRUE;
	}
	else {
		if ($pass1 !== $pass2){		// !== not identical operator, not only the value but also the type must be equal
			$pass2Err = "<span class=\"form_error\">Passwords are not the same</span>";
			$err = TRUE;
		}
	}
	   //INSTITUTION
	if (empty($affiliation)){
		$affiliationErr = "<span class=\"form_error\">Institution is required</span>";
		$err = True;
	}elseif ($affiliation === "Other"){
		if (empty($other_affiliation)){
			$other_affiliationErr = "<span class=\"form_error\">Institution is required</span>";
			$err = True;
		} elseif($other_affiliationLength > 100){
			$other_affiliationErr = "<span class=\"form_error\">Institution must be less than 100 characters long</span>";
			$err = TRUE;
		}
	}
  // Position entry Error
  // 1. Check if emtpy
  // 2. If other is selected, check that the optional
  //    box is filled
  if (empty($position)){
    $positionErr = "<span class=\"form_error\">Position is required</span>";
    $err = True;
  }elseif ($position === "Other_P"){
    if (empty($other_position)){
      $other_positionErr = "<span class=\"form_error\">Position is required</span>";
      $err = True;
    } elseif($other_positionLength > 100){
      $other_positionErr = "<span class=\"form_error\">Position must be less than 100 characters long</span>";
      $err = TRUE;
    }
  }

  if (empty($source) or $source === "empty"){
    $other_sourceErr = "<span class=\"form_error\">Please choose one option</span>";
    $err = True;
  }elseif ($source === "Other"){
    if (empty($other_source)){
      $other_sourceErr = "<span class=\"form_error\">Please write from where did you find us</span>";
      $err = True;
    } elseif($other_positionLength > 100){
      $other_sourceErr = "<span class=\"form_error\">This entry must be less than 100 characters long</span>";
      $err = TRUE;
    }
  }



    // CAPTCHA
    if ($captcha !==  $_SESSION['captcha']['code']){
        $captchaErr = "<span class=\"form_error\">Wrong captcha</span>";
        $err = TRUE;
    }

    $_SESSION['Errors'] = array(
                                'firstnameErr'         => $firstnameErr,
                                'lastnameErr'          => $lastnameErr,
                                'emailErr'             => $emailErr,
                                'pass1Err'             => $pass1Err,
                                'pass2Err'             => $pass2Err,
                                'captchaErr'           => $captchaErr,
                                'other_affiliationErr' => $other_affiliationErr,
                                'other_positionErr'    => $other_positionErr,
                                'other_sourceErr'	   => $other_sourceErr,
                                'errormessage' => ''
                                );

	if (!$err){		// IF THE ENTRIES HAVE SUITABLE VALUES CONNECT TO DATABASE
	    include('ConnectDB.php');
	    if ($affiliation === "Other"){
			    $affiliation = $other_affiliation;
		  }
      if ($position === "Other_P"){
          $position = $other_position;
      }

      if ($source === "Other"){
          $source = $other_source;
      }
		// TRANSFORM THE ENTRY VARIABLES TO SQL FRIENDLY FORMAT
		$firstname = sql_friendly($firstname, $con);
		$lastname = sql_friendly($lastname, $con);
		$email = sql_friendly($email, $con);
		$pass1 = sql_friendly($pass1, $con);
		$pass2 = sql_friendly($pass2, $con);
		$affiliation = sql_friendly($affiliation, $con);
    	$position = sql_friendly($position, $con);
    	$source = sql_friendly($source, $con);

		//=============================================================
		// CHECK THAT THE EMAIL EXISTS. IF POSITIVE, SHOW AN ERROR.
		//	IF NOT, WRITE THE INPUT INFORMATION TO THE TABLE
		//==============================================================
		$query = "SELECT * FROM SUBSCRIBERS WHERE Email=$email";
		$result=$con->query($query);

		if ($result === FALSE){
        	$errorMessage =  "Error: ". $con->error;	//error in executing the SQL code in the database
        	error_write($_SERVER['REMOTE_ADDR'] . "  Error: addPersonf, line190: " . $query . "<br>" . $con->error);
      	} else {
      		$row = $result -> fetch_array(MYSQLI_ASSOC);
      		if ($row > 0) {
				$_SESSION['Errors']['errormessage'] = "The Email address is already registered";
                $con->close();
                header("Location: SignUp.php");
			}
			else {
			    // Generate a unique key here store in an variable $Key
        		$key = sql_friendly(bin2hex(openssl_random_pseudo_bytes(20)), $con);
        		$query = "INSERT INTO SUBSCRIBERS (FirstName, LastName, Email, Pass, Affiliation, Position, Source, PassKey) VALUES ($firstname, $lastname, $email, md5($pass1), $affiliation, $position, $source, $key)";

				if ($con->query($query) === TRUE) {
    				echo "New record created successfully\r\n";

		              // EMAIL CONFIRMATION ====================================================
		            $to = str_replace("'", "", $email);
		            $subject = "Confirmation from SCCS 2015";
		            $txt = "Dear " . str_replace("'", "", $firstname) . " " . str_replace("'", "", $lastname) . "," . "\r\n\r\n";
		            $txt .= "please click the link below to verify and activate your account:" . "\r\n\r\n";
		            $txt .= "http://sccs2015.soton.ac.uk/confirm.php?ID=" . str_replace("'", "", $key) . "\r\n\r\n\r\n";
		            $txt .= "Kind regards," . "\r\n" . " the ICCS 2015 Team " . "\r\n\r\n";
		            $headers = "From: noreply@sccs2015.soton.ac.uk" . "\r\n";
		            $headers .= "MIME-Version: 1.0" . "\r\n";
		            $headers .= "Content-type:text;charset=UTF-8" . "\r\n";
		            $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

		            $sendmail = mail($to, $subject, $txt, $headers);
		            if ($sendmail){
		            	echo "Your Confirmation link has been sent to your email address.";
		            }else{
		            	echo " Cannot send confirmation link to your email address.";
		            }
          // =======================================================================


				//==========================================================================
				//	START THE SESSION AND SET THE SESSION VARIABLE TO 1, SO THAT THE USER
				//  HAS ACCESS TO RESTRICTED PAGES. REDIRECT THE USER TO HER/HIS HOMEPAGE
				//==========================================================================
					// $_SESSION['SignIn'] = "1";
	    //             $_SESSION['firstname'] = $firstname;
	    //             $_SESSION['lastname'] = $lastname;
	    //             $_SESSION['email'] = $email;
					//header ("Location: tabs_login.php");

				} else {
    				$errorMessage = "Error: " . $query . "<br>" . $con->error;
    				error_write($_SERVER['REMOTE_ADDR'] . "  Error: addPersonf, line 239: " . $query . "<br>" . $con->error);
            		
				}
				$con->close();

			}
		}
	} else {
        header("Location: SignUp.php");
    }

	// return array($errorMessage, $firstnameErr,	$lastnameErr, $emailErr, $pass1Err, $pass2Err, $captchaErr);

}

?>
