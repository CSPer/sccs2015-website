<?
function LogIn()
{
    // Selects rows in SUBSCRIBERS table that matches with the user inputted email and password.
    $errorMessage = "";
    include("ConnectDB.php");
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php"); // Errors log file
    
    // Check that email box is not empty
	if(!empty($_POST['email']))
	{
		$email = test_input($_POST["email"]);
		$pass = test_input($_POST["pass"]);
        $email = sql_friendly($email, $con);
        $pass = sql_friendly($pass, $con);

        // Check through all the email and passw column entries from the database
        // and get the information from the entries whose email and password match
        // the ones used in the inputs (this can be empty if the
        // information is not registered in the table)
  		$query = "SELECT * FROM SUBSCRIBERS where Email = $email AND Pass = md5($pass) AND Activation=1";
  		// Send the sql code to database. The result is a binary
        // where is True if the connection succeeded
        // (we can still have empty entries even if the connection succeeded)
        $result=$con->query($query);

        if ($result === FALSE) {
            // Error in executing the SQL code in the database
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: loginf, line 30: " . $query . "<br>" . $con->error);
        }
        else {
            // Get all the entries from the corresponding row
            $row = $result -> fetch_array(MYSQLI_ASSOC);

            // Now check that the entries are not empty
            if(!empty($row['Email']) AND !empty($row['Pass']))
            {
                // From the session_start() , get the variables
                $_SESSION['SignIn'] = "1";
                $_SESSION['email'] = test_output_sql($row['Email']);
                $_SESSION['firstname'] = test_output_sql($row['FirstName']);
                $_SESSION['lastname'] = test_output_sql($row['LastName']);
                $_SESSION['abstract'] = test_output_sql($row['Abstract']);
                $_SESSION['abstract_title'] = test_output_sql($row['AbstractTitle']);
                $_SESSION['presentation'] = test_output_sql($row['Presentation']);
                $_SESSION['affiliation'] = test_output_sql($row['Affiliation']);
                $_SESSION['position'] = test_output_sql($row['Position']);
                $_SESSION['source'] = test_output_sql($row['Source']);
                $_SESSION['keywords'] = test_output_sql($row['Keywords']);
                $_SESSION['assistance'] = test_output_sql($row['Assistance']);
                $_SESSION['trip'] = test_output_sql($row['Trip']);
                $_SESSION['diet'] = test_output_sql($row['Diet']);
                $_SESSION['dietReq'] = test_output_sql($row['DietReq']);
                
                // Save the IP address of the session
                $ip_address = sql_friendly($_SERVER['REMOTE_ADDR'], $con);
                $query = "UPDATE SUBSCRIBERS SET IP=$ip_address  WHERE email=$email";
                if ($con->query($query) === FALSE) { 
                     // Write to the error file
                    error_write($_SERVER['REMOTE_ADDR'] . "  Error: loginf, line 56: " . $query . "<br>" . $con->error);
                }

                // Show the personalised message for ICSS students
                // The classes are defined in the CSS file, these variables
                // echoes the message in the given class so they appear only for
                // ICSS students
                if ($_SESSION['affiliation'] === "ICSS_DTC"){
                    $_SESSION['affiliation_information'] = "<div class=\"affiliation-info\"> As an ICSS student, your flight and accomodation will be arranged by the ICSS </div>";
                    $_SESSION['payment_information'] = "<div class=\"payment-info\">As an ICSS student no payment is required for registration </div>";
                }
                else {
                    $_SESSION['affiliation_information'] = '';
                    $_SESSION['payment_information'] = '';
                }
                header("Location: ./tabs_login.php");
            } else {
                $errorMessage = "<div class=\"login_error\"><span>Wrong email or <br> password</span></div>;";
                $_SESSION['SignIn'] = '';
                session_destroy();
            }
        }
    }
    $con->close();
    return $errorMessage;
}





?>
