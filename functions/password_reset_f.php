<?
function SendPassRes()
{
    // Selects rows in SUBSCRIBERS table that matches with the user inputted email and password.
    $errorMessage = "";
    include("ConnectDB.php");
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php"); // Errors log file

    // Check that email box is not empty
	if(!empty($_POST['email']))
	{
		$email = test_input($_POST["email"]);
        $email = sql_friendly($email, $con);
        
        // Check through all the email and passw column entries from the database
        // and get the information from the entries whose email and password match
        // the ones used in the inputs (this can be empty if the
        // information is not registered in the table)
  		$query = "SELECT * FROM SUBSCRIBERS WHERE Email = $email AND Activation=1";
  		// Send the sql code to database. The result is a binary
        // where is True if the connection succeeded
        // (we can still have empty entries even if the connection succeeded)
        $result=$con->query($query);
        
        if ($result === FALSE) {
            // Error in executing the SQL code in the database
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: sendPass, line 28: " . $query . "<br>" . $con->error);
            
        }
        else {
            // Get all the entries from the corresponding row
            $row = $result -> fetch_array(MYSQLI_ASSOC);

            // Now check that the entries are not empty
            if(!empty($row['Email']))
            {

                // EMAIL CONFIRMATION ====================================================
                $to = str_replace("'", "", $email);
                $subject = "Reset Password from SCCS 2015 account";
                $txt = "Dear " . str_replace("'", "", $row['FirstName']) . " " . str_replace("'", "", $row['LastName']) . "," . "\r\n\r\n";
                $txt .= "please click the link below to reset your password and specify a new email:" . "\r\n\r\n";
                $txt .= "http://sccs2015.soton.ac.uk/password_reset_confirmation.php?ID=" . str_replace("'", "", $row['PassKey']) . "\r\n\r\n\r\n";
                $txt .= "Kind regards," . "\r\n" . " the ICCS 2015 Team " . "\r\n\r\n";
                $headers = "From: noreply@sccs2015.soton.ac.uk" . "\r\n";
                $headers .= "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text;charset=UTF-8" . "\r\n";
                $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

                $sendmail = mail($to, $subject, $txt, $headers);
                if ($sendmail){
                    // echo "Your link has been sent to your email address.";
                    $errorMessage = "<div class=\"password_error\" style=\"background: #0e4108;\"><span>A link has been <br/> sent to your email address</span></div>;";
                }else{
                    // echo " Cannot send link to your email address.";
                    $errorMessage = "<div class=\"password_error\"><span>The link could not be <br/> sent to your email address</span></div>;";
                }

                // header("Location: ./Login.php");
            } else {
                $errorMessage = "<div class=\"password_error\"><span>Email not registered <br> or <br> Account not activated</span></div>;";
                $_SESSION['SignIn'] = '';
                session_destroy();
            }
        }
    }
    $con->close();
    return $errorMessage;

}

function PassRes($key)
{
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php"); // Errors log file

    // Initialize variables before they are going to be assigned
    $err = FALSE;
    $keyErr="";
    
    $pass1Err="";
    $pass2Err="";
    
    $pass1 = test_input($_POST["pass1"]);
    $pass2 = test_input($_POST["pass2"]);
    
    $passLength  = strlen($pass1);

        // PASS1 -------------------------
        // Fail if it is empty
    if (empty($pass1)){
        $pass1Err = "<span class=\"form_error\">Password is required</span>";
        $err = TRUE;
    }
        // Fail if is is longer than x characters
    else {
        if ($passLength > 20){
            $pass1Err = "<span class=\"form_error\">Password must be less than 20 characters long</span>";
            $err = TRUE;
        }
    }
        // PASS2 -------------------------------
        // The same
    if (empty($pass2)){
        $pass2Err = "<span class=\"form_error\">Re-type password is required</span>";
        $err = TRUE;
    }
    else {
        if ($pass1 !== $pass2){		// !== not identical operator, not only the value but also the type must be equal
            $pass2Err = "<span class=\"form_error\">Passwords are not the same</span>";
            $err = TRUE;
        }
    }
    
    // If the error was well written, pass to the main statement
    if (!$err){
        // Connect to the databse
        include('ConnectDB.php');
        // Filter the password and make it friendly to pass it to the database
        $pass1 = sql_friendly($pass1, $con);
        // Get the row with the same KEY (if it fails, the row is empty)
        $query = "SELECT * FROM SUBSCRIBERS WHERE PassKey = '$key'";

        // Execute the command to pass it to the database
        // This should return an array , not a Boolean, so
        // we check first if it is false
        $result=$con->query($query);
        
        if ($result === FALSE) {
            // If the command passed to the database (the 
            // one to get a row where email matches) failed,
            // write the error to the php error file
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: PassRes, line 123: " . $query . "<br>" . $con->error);
        }
        else{
            // Get all the entries from the corresponding row
            $row = $result -> fetch_array(MYSQLI_ASSOC);

            // Now check that the entries are not empty
            if(empty($row['Email']))
            {   
                // Write the error to the PHP file if the row was empty
                error_write($_SERVER['REMOTE_ADDR'] . "  Error: PassRes Empty KEY entry, line 120: " . $query . "<br>" . $con->error);
            }
            else{
                // If the row is not empty, update the password
                // The UPDATE command returns a Boolean
                $query = "Update SUBSCRIBERS SET Pass=md5($pass1) Where PassKey='$key'";
                // Execute the command to the database. If it passes, show
                // a meesage in green
                if ($con->query($query) === TRUE) {
                    $keyErr="<div class=\"password_error\" style=\"background: #0e4108; margin-top: 80px;\"><span>Password changed successfully! </span> <br/><br/> You may now <br/> <a style=\"color: #da7100;\"href=\"Login.php\">Log in</a> </div>;";
                }
            }
        }
        $con->close();
    }
    // Return the password errors (red boxes or empty strings) and
    // the keyErr which shows if the password was successfully updated
    return array($pass1Err, $pass2Err, $keyErr);
        
}
?>
