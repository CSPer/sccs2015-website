<?
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function test_output($data) {
   	$data=preg_replace('/(^[\']|[\']$)/', '', $data);
    $data = str_replace( "\\'", "'", $data);
	return $data;
}

function test_output_sql($data) {
    $data = str_replace( "\\'", "'", $data);
	return $data;
}


function sql_friendly($data, $handle) {
   if (!is_numeric($data)) {
       $data = "'".$handle->real_escape_string($data)."'";
   }
   return $data;
}

?>

