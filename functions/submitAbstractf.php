<?

function SubmitAbstract(){
    
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php"); // write errors to the file

    $err = FALSE;
    $abstract = test_input($_POST['abstract']);
    $abstract_title = test_input($_POST['abstract_title']);
    $presentation = test_input($_POST['presentation']);
    $keywords = test_input($_POST['keywords']);
    $keywordsLen = str_word_count($keywords,0);
    $abstractLen = str_word_count($abstract, 0);
    $abstract_titleLen = str_word_count($abstract_title, 0);
    $email = $_SESSION['email'];

    // If the abstract and the title are longer than the desired values,
    // store the current values and return them. Do not write
    // to the database (we check this with the $err variable)
    if($abstractLen > 350){
        $_SESSION['abstractErr'] = "<div class=\"error-submission\"> Your abstract must have no more than 350 words</div>";
        $_SESSION['abstract'] = $abstract;
        $_SESSION['abstract_title'] = $abstract_title;
        $_SESSION['presentation'] = $presentation;
        $_SESSION['keywords'] = $keywords;
        $err = TRUE;
    }
    if($abstract_titleLen > 20){
        $_SESSION['abstract_titleErr'] = "<div class=\"error-submission\">Your abstract title must not have more than 20 words </div>";
        $_SESSION['abstract'] = $abstract;
        $_SESSION['abstract_title'] = $abstract_title;
        $_SESSION['presentation'] = $presentation;
        $_SESSION['keywords'] = $keywords;
        $err = TRUE;
    }

    if($keywordsLen > 4){
        $_SESSION['keywordsErr'] = "<div class=\"error-submission\">Keywords must have no more than 4 words</div>";
        $_SESSION['abstract'] = $abstract;
        $_SESSION['abstract_title'] = $abstract_title;
        $_SESSION['presentation'] = $presentation;
        $_SESSION['keywords'] = $keywords;
        $err = TRUE;
    }

    if(!empty($abstract) && $presentation === "None"){
        $_SESSION['presentationErr'] = "<div class=\"error-submission\">Preferred presentation type not selected </div>";
        $err = TRUE;
        
    }

    include('ConnectDB.php');

    $abstract = sql_friendly($abstract, $con);
    $abstract_title = sql_friendly($abstract_title, $con);
    $email = sql_friendly($email, $con);
    $presentation = sql_friendly($presentation, $con);
    $keywords = sql_friendly($keywords, $con);
    $query = "UPDATE SUBSCRIBERS SET Abstract=$abstract, AbstractTitle=$abstract_title, Presentation=$presentation, Keywords=$keywords WHERE email=$email";
    
    if(!$err){
        // If the information was successfully recorded, send the
        // information back to show it in the Information tab page, using
        // the current SESSION variable
        if ($con->query($query) === TRUE) {
            $_SESSION['abstract'] = test_output($abstract);
            $_SESSION['abstract_title'] = test_output($abstract_title);
            $_SESSION['presentation'] = test_output($presentation);
            $_SESSION['keywords'] = test_output($keywords);
            $_SESSION['abstractErr'] = "<script type='text/javascript'>alert(\"Abstract submitted successfully\");</script>";
            $_SESSION['abstractErr2'] = "<div class=\"successful-submission\"> Abstract submitted successfully </div>";
        } else {
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: submitAbstractf, line 74: " . $query . "<br>" . $con->error);
            // echo "Error: " . $query . "<br>" . $con->error;
        }
        $con->close();
    }
}
?>
