<?

function SubmitInfo(){
    include("functions/safety.php"); // includes test_input() and sql_friendly() functions
    include("functions/error_write.php");
     $err = FALSE;
    $firstname = test_input($_POST['firstname']);
    $lastname = test_input($_POST['lastname']);
    $source = test_input($_POST['source']);
    $other_source = test_input($_POST['other_source']);
    //$affiliation = test_input($_POST['affiliation']);
    
    // Lengths to avoid information overflows
    $firstnameLen = strlen($firstname);
    $lastnameLen = strlen($lastname);
    //$affiliationLen = strlen($affiliation);
    
    if ($firstnameLen > 20){
        echo "Name must be shorter than 20 characters";
    }
    elseif($lastnameLen > 20){
        echo "Last name must be shorter than 20 characters";
    }

    if ($source === "Other"){
        if (empty($other_source)){
            $_SESSION['sourceErr'] = "<div class=\"error-submission\"> ''Where did you find us?'' is not specified. Please choose one option.</div>";
            $err = True;
        }
    }

    //elseif($affiliationLen > 60){
    //    echo "Institution name must be shorter than 60 characters";
    //}

    if (!$err){
        $email = $_SESSION['email'];

        include('ConnectDB.php');

        if ($source === "Other"){
            $source = $other_source;
        }
        $source = sql_friendly($source, $con);
        $firstname = sql_friendly($firstname, $con);
        $lastname = sql_friendly($lastname, $con);
        //$affiliation = sql_friendly($affiliation, $con);
        $email = sql_friendly($email, $con);
        $query = "UPDATE SUBSCRIBERS SET FirstName=$firstname, LastName=$lastname, Source=$source  WHERE email=$email";
        //$query = "UPDATE SUBSCRIBERS SET FirstName=$firstname, LastName=$lastname, Affiliation=$affiliation  WHERE email=$email";
        
        // If the information was successfully recorded, send the
        // information back to show it in the Information tab page, using
        // the current SESSION variable
        if ($con->query($query) === TRUE) { 
            $_SESSION['firstname'] = test_output($firstname);
            $_SESSION['lastname'] = test_output($lastname);
            $_SESSION['source'] = test_output($source);
            // $_SESSION['affiliation'] = $affiliation;

        } else {
            error_write($_SERVER['REMOTE_ADDR'] . "  Error: submitinfof, line 46:  " . $query . "<br>" . $con->error);
            // echo "Error: " . $query . "<br>" . $con->error;
        }

        $con->close();
    }
}

?>
