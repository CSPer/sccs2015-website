<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Granada Information";
	$pageDescription = "The SCCS 2014 conference will be hosted on the Falmer campus of the University of Sussex.";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>
		<?php include("includes/pageTitle.php"); ?>
    	
        
        <?php include("includes/navigation.php"); ?>
        <!-- <?php include("includes/ComingSoon.php"); ?>  -->
        
        <div class="content">
            <div class='granada-subs'>
                HOTELS
            </div>
             
           
            <div class="granada-list">
            <ul>
                <li><span><a href="http://www.hostalbocanegra.com">Hostal Boca Negra</a></span></li>
 					One of the cheapest private-room accommodations. 
                    Highly recommended for people on a budget. They are a 15 minute
                    walk away from the conference venue.
                    Prices start at 10 Euros/Night for a double (with shared bathroom) 
                    or 24 Euros/Night for a double with private bathroom. Free Wifi.
                    Laundry Service. 5 minutes away from the city centre and popular
                     local bars.

                <li><span><a href="http://www.maciahoteles.com">Hotel Maciá Monasterio De Los Basilios</a></span></li>
 					
                    5 Minute walk away from the conference venue. Mid-range option. 
                    Prices go between 50 and 65 Euro/Night. Free Wifi. 
                    In-situ "arab baths" (hot water indoor pools).
                    
                <li><span><a href="http://www.senatorgranadaspahotel.com">Hotel Senator Granada Spa</a></span></li>
                    
                    High in the price range. Just a 5 minute walk from/to the venue. 
                    Prices vary between 65 and 90 Euros/Night depending on room types and early booking discounts. 
                    In-situ Spa costs 16 Euros/Person  per session (Booking Required). 
                    Free Standard wifi connection (high-speed optional, at cost). 
                    
                <li><span><a href="http://hotelgranadasaray.com/en/">Hotel Saray</a></span></li>
                    
                    A comfortable and rather expensive option. 
                    Expect to pay between 85 and 100 Euros/Night for a room. 
                    Ideal location right in front the conference venue. 
                    Pool. Free wifi with online booking.

            </ul>
            </div>
            
        <div class='granada-subs'>
                RESTAURANTS
            </div>
             
             The north-eastern region from the venue has a high concentration of 
             close-by restaurants. Further North (above the Genil river) there is a
              great number of them. Between 10 and 20 minutes away, walking. 
              Here's a short assorted list of restaurants in the area:

           
            <div class="granada-list">
            <ul>
                <li><span>Restaurante El Ventorrillo</span></li>
 					Convenient location. Good ratings and excellent service. 
                    Bar and Grill. Less than  5 minutes away from the venue.

                <li><span>La Lonja de Granada</span></li>
 					
                    Great "tapas" variety, excellent service. Friendly atmosphere.
                    Less than 5 minutes from the venue.
                    
                <li><span><a href="http://www.restaurantechikito.com/">El Chikito</a></span></li>
                    
                    Regional cuisine, Traditional Andalusian/Arab. 12 minutes away from the venue. 
                
                <li><span><a href="http://www.cantinamexicanachilegrande.com/">Cantina Mexicana Chile Grande</a></span></li>
                    
                    Mexican Food, Spicy and colourful. 12 minutes away from the venue. 
                    
                <li><span><a href="http://www.restauranteraices.es/">Restaurante Raices</a></span></li>
                
                Vegetarian Restaurant. Daily Menu, Fresh Food. 9 minutes from the venue. 

            </ul>
            </div>
        
        <div style="margin-top: 40px;">
        <center>
            <iframe src="https://www.google.com/maps/d/u/0/embed?mid=zYjPPzbiN1Ak.kw_DeGVcLbz8" width="640" height="480"></iframe>

        </center>
        </div>
        
    </div>

<?php include("includes/bodyBottom.php"); ?>
   


</body>
</html>
