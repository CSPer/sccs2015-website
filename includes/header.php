<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>SCCS 2015 - <?php echo $pageTitle?></title>

<!-- ICON -->
<link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />

<?php
	if (!isset($pageDescription)) {
		$pageDescription = "SCCS 2015 Student conference web site";
	}
	
	if (!isset($pageKeywords)) {
		$pageKeywords = "sccs,complexity,science";
	}
?>

<META NAME="DESCRIPTION"
CONTENT="<?php echo $pageDescription?>" />
<META NAME="KEYWORDS"
CONTENT="<?php echo $pageKeywords?>" />

<link href="css/main.css" type="text/css"  rel="stylesheet" media="all" />
<link href="css/print.css" type="text/css"  rel="stylesheet" media="print" />

</head>
