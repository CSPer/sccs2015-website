<?php
	//-- find the page name
	$whereLastSlash = strpos($_SERVER['REQUEST_URI'],"/",1)+1;
	$pageName = substr($_SERVER['REQUEST_URI'],$whereLastSlash,strlen($_SERVER['REQUEST_URI'])-$whereLastSlash);
	if (strlen($pageName) <= 0) {
		$pageName = "index.php";
	}
?>
        
        <div class="navigation blueLeft block">
            <div class="mainMenu block">
		<ul>
		<!-- These elements are HTML links in the left menu, in an unordered list. When clicked, the 
		     pageName variable changes according to the webpage name and the buttons change
		     to the class 'selected' (which has a lighter colour) --> 
                    <li <?php if ($pageName == "index.php") { echo ('class="selected"'); } ?>><a href="index.php">Home</a></li>
                    <li <?php if ($pageName == "keynotes.php") { echo ('class="selected"'); } ?>><a href="keynotes.php">Keynotes</a></li>
                    <li <?php if ($pageName == "research_themes.php") { echo ('class="selected"'); } ?>><a href="research_themes.php">Themes</a></li>
                    <li <?php if ($pageName == "workshops.php") { echo ('class="selected"'); } ?>><a href="workshops.php">Workshops</a>
                    <!--
                        <?php if ($pageName == "worksshop.php") { ?>
                        <ul class="subMenu">
                        	<li><a href="workshops.php#complexity">Complexity, Evolution and the Origin Of Life</a></li>
                            <li><a href="workshops.php#fdm">Finite Difference Modelling</a></li>
                            <li><a href="workshops.php#software">Software Project Planning &amp; Management</a></li>
                        	<li><a href="workshops.php#validation">The Power of Model Validation</a></li>
                        	<li><a href="workshops.php#productivity">Tools From a Productivity Toolbox</a></li>
                            
                        </ul>
						<?php } ?>
                    -->
                    </li>
                    <li <?php if ($pageName == "programme.php") { echo ('class="selected"'); } ?>><a href="programme.php">Programme</a>
                    <!--
                        <?php if ($pageName == "programme.php") { ?>
                        <ul class="subMenu">
                        	<li><a href="programme.php#workshops1">Workshops Session 1</a></li>
                            <li><a href="programme.php#workshops2">Workshops Session 2</a></li>
                            <li><a href="programme.php#presentations1">Presentations Session 1</a></li>
                            <li><a href="programme.php#presentations2">Presentations Session 2</a></li>
                            <li><a href="programme.php#presentations3">Presentations Session 3</a></li>
                            <li><a href="programme.php#presentations4">Presentations Session 4</a></li>
                            <li><a href="programme.php#presentations5">Presentations Session 5</a></li>
                        </ul>	
                        <?php } ?>
                    -->
                    </li>
                    <li <?php if ($pageName == "granada_info.php") { echo ('class="selected"'); } ?>><a href="granada_info.php">Granada</a>
                    <!--	
                        <?php if ($pageName == "granada_info.php") { ?>
                        <ul class="subMenu">
                        	<li><a href="venue.php#conference">Conference Venue</a></li>
                            <li><a href="venue.php#dinner">Dinner Venue</a></li>  
                        </ul>
						<?php } ?> 
                    </li>
                    -->
                    
                    <!--<li <?php if ($pageName == "funDay.php") { echo ('class="selected"'); } ?>><a href="funDay.php">Fun Day</a></li>  -->                  
                    <li <?php if ($pageName == "contact.php") { echo ('class="selected"'); } ?>><a href="contact.php">Contact</a></li>
                </ul>
            </div>
        </div>
