<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Welcome to SCCS 2015";
    
    $pageDescription = "The Student Conference on Complexity Science (SCCS) is 
        the largest UK conference for early-career researchers working under 
        the interdisciplinary framework of Complex Systems, with a particular 
        focus on computational modelling, simulation and network analysis. 
        Since 2010, this conference series has brought together PhD students 
        and early career researchers from both the UK and overseas, whose 
        interests span areas as diverse as quantum physics, ecological food 
        webs or the economics of happiness. This interdisciplinary nature of 
        the conference is reflected by the diversity of keynote speakers as 
        well as practical, hands-on workshops";
    
    //--------------------------
?>

<?php include ("includes/header.php"); ?>


<body>
<?php include_once("analyticstracking.php") ?>
<?php include("includes/bodyTop.php"); ?>
<?php include("includes/pageTitle.php"); ?>


        <?php include("includes/navigation.php"); ?>

        <div class="content">

        	<div class="block">
                <div class="figure bordered margin-left">
                    <img src="images/art/deep_waters_Tessa.jpg" width="350" height="350" border="0" 
                    alt="Student Conference on Complexity Science 2015" />
                    <p>&copy; <a href="http://www.tessacoe.co.uk/" target="_blank">Tessa Coe 2014</a></p>
                    
                    <br>
                    <div style="font-size: 10pt; font-weight: bold; text-align: left; border: 2px solid #005C84; padding: 5px 5px;">
                    <span>
                    Upcoming Summer School in Unconventional Computing
                    </span> 
                    <a href="http://www.truce-project.eu/2015-summer-school.html" target="_blank">
                    <img src="images/truce_logo.png" width="340" height="70" border="0" 
                    alt="Student Conference on Complexity Science 2015" /></a>
                    <br>August 31-September 4, 2015 - Málaga, Spain
                    </div>
                    
                </div>   
                
                <div class="announcement" onclick="location.href='Login.php';" >
                    Registration and payment <br>  is closed
                </div>
                
                <div class="paragraph">
                    The UK’s largest conference for early-career researchers in complexity science is graduating to mainland Europe.
                    The Student Conference on Complexity Science (SCCS) has a focus on computational modelling, simulation and network analysis 
                    that extends over the full range of disciplines. The interdisciplinary nature of the conference is attested by its diversity
                    of keynote speakers, delegates, and practical, hands-on workshops. <br> <br>
                    Since 2010, the SCCS conference series has brought together PhD students and early-career researchers from the UK and beyond,
                    whose interests span areas as diverse as quantum physics, ecological food webs, and the economics of happiness.
                    This year SCCS will take place for three days in Granada, Spain to engage more international participation.
                </div>
 
                <div style="float: right; width: 350px;">
                        
                    <span style="float:left; font-weight:bold;"> SPONSORED BY </span>
                    <img src="images/sponsors/NESS_logo.png" style="width:180px; vertical-align:middle; float:right;"/>
                    <br><br>
                    <img src="images/sponsors/EPSRC_logo.jpg" style="width:180px; vertical-align:top; float:left;"/>
                    <img src="images/sponsors/GSS_logo.png" style="width:160px; vertical-align:top; float:right;"/>
                </div> 
               
              
            </div>
    		<div style="float: left; width: 370px; text-align: left; margin-top: 15px;" class="margin-top">
                 <!-- Poster -->
                
                <h2>Download our poster:</h2>
                <div style="margin-left: 5%">
                    <a href="downloads/sccs2015Poster.pdf" target="_blank"><img src="images/sccs2015Poster.jpg" width="250" height"354" border="0" class="border margin-left-larger" alt="SCCS 2015 poster"  /></a>
                </div>
                <br><br>
                
                <div class="block">
                    <h2>Conference Venue:</h2>
                    <!--
                    Presentations and posters announced:<br />
                    &nbsp;&nbsp;&nbsp;19th May 2014<br /><br />
                -->
                    <strong>SCCS 2015 will be held between 9-11 September 2015 in 
                    <a href="http://www.pcgr.org/en/" target="_blank">Granada Congress and Exhibition Centre</a>, Spain.</strong><br />
                    <!--
                    <br /><br />
                     <img src="http://www.pcgr.org/en/palacio/banner_animated_en.gif" width="370" ></img> 
                    <br /><br /> -->
                    <br/>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2550.192022581662!2d-3.5990824232788077!3d37.16553334102144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd71fca304958221%3A0xab683a46be9a1163!2sPalacio+de+Congresos%2C+18006%2C+Granada%2C+Spain!5e1!3m2!1sen!2suk!4v1424035093648" width="370" height="315" frameborder="0" style="border:0"></iframe>               
                    <!--
                    <h2>Download our posters:</h2>
                    <a href="downloads/sccs2014Poster_red.pdf" target="_blank"><img src="images/poster_red.jpg" width="140" border="0" class="bordered margin-right-smaller" alt="SCCS 2014 poster - red"  /></a><a href="downloads/sccs2014Poster_green.pdf" target="_blank"><img src="images/poster_green.jpg" width="140" border="0" class="bordered" alt="SCCS 2014 poster - green"  /></a>
                -->
                </div>
            </div>
            <div style="float: right; width: 350px;" class="margin-top">
            	<!-- The twitter widget from 2014 sccs
                <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/sccs15"  data-widget-id="408339965752250368">Tweets by @sccs2015</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			    -->
            <a class="twitter-timeline" height="862" href="https://twitter.com/sccs15" data-widget-id="565940257267658752">Tweets by @sccs15</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

            </div>

            <br /><br /><br /><br />

        </div>

<?php include("includes/bodyBottom.php"); ?>



</body>
</html>
