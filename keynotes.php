<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
    //---- PAGE SETTINGS -------
    $pageTitle = "Keynotes";
    $pageDescription = "The Student Conference on Complexity Science 2014 keynotes: Professor Nigel Gilbert, Professor Henrik Jeldtoft Jensen, Professor Mark Newman, Professor E&ouml;rs Szathm&aacute;ry";
    //--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>
        <?php include("includes/pageTitle.php"); ?>
        
        
        <?php include("includes/navigation.php"); ?>

        <!-- <?php include("includes/ComingSoon.php"); ?> -->

        <div class="content">
            <div class="keynote margin-bottom-larger">
                <div class="image">
                    <img src="images/keynote_feigenbaum.jpg" width="150" 
                     height="120" border="0" class="bordered" />
                </div>
                <div class="text">
                    <h2>Professor Mitchell Feigenbaum,<br />The Rockefeller University, USA</h2>

                    Professor Mitchell Jay Feigenbaum is a pioneer
                    in the science of chaos. Insofar as physics is 
                    about the quest for understanding, Professor 
                    Feigenbaum's work is in the grand tradition of 
                    physics and has a universality that cuts across 
                    disciplines. His work has applications in a wide range of fields including one-
                    dimensional quadratic maps, complex chemical reactions, the onset of 
                    turbulence in convecting fluids under appropriate conditions, and the dynamics 
                    of growth of competing biological populations. The impact of his discoveries has 
                    been phenomenal and his observations are at the heart of theoretical limitations 
                    to the predictive power of science. 
                    His awards include the Dannie Heineman Prize for Mathematical Physics for 
                    developing the theory of deterministic chaos, a New York City Mayor’s Award for 
                    Excellence in Science and Technology for his pioneering studies in chaos theory, 
                    Israel’s top scientific honor, the Wolf Foundation Prize in Physics, a MacArthur 
                    Foundation Fellowship, the Ernest O. Lawrence Award by the United States 
                    Department of Energy, amongst others.
                 
                </div>
            </div>
            
            <div class="keynote margin-bottom-larger">
                <div class="image">
                    <img src="images/keynote_reynolds.jpg" width="150"
                     height="150" border="0" class="bordered" />
                </div>
                <div class="text">
                    <h2>Craig Reynolds,<br />Staples SparX, USA</h2>
    
                    Craig Reynolds is an artificial life and computer
                    graphics expert. His interests center around using 
                    computer programs to simulate complex natural 
                    phenomenon. These models can aide scientific 
                    understanding of the natural system. He is probably 
                    best known for his work on modeling coordinated 
                    animal motion such as bird flocks and fish schools. 
                    His most recent work involves using evolutionary computation to model the 
                    evolution of camouflage in nature.
                    Reynolds won the 1998 Scientific And Engineering Award presented by The 
                    Academy of Motion Picture Arts and Sciences for pioneering contributions to the 
                    development of three dimensional computer animations for motion picture 
                    production. He is a frequent reviewer for a number of conferences and journals 
                    and has served on the editorial boards of Artificial Life and Genetic Programming 
                    and Evolvable Machine.
                 
                </div>
            </div>
            
            <div class="keynote margin-bottom-larger">
                <div class="image">
                    <img src="images/keynote_johnson.jpg" width="150"
                     height="190" border="0" class="bordered" />
                </div>
                <div class="text">
                    <h2>Professor Jeffrey Johnson,<br />The Open University, UK</h2>
    
                        Professor Jeff Johnson is Professor of Complexity 
                        Science and Design at the Open University. As a 
                        member of the Global Systems Dynamics and Policy 
                        Consortium, he is particularly interested in the 
                        application of new scientific methods in global 
                        systems dynamics to address policy issues, and is 
                        working towards making links between science and 
                        its application to large complex policy problems in 
                        the private and public sectors.
                        Professor Johnson has written major Computer Aided Learning packages and has 
                        various awards for his teaching including a British Computer Society medal for 
                        innovation in computer-aided learning. He is a Fellow of the Institute of 
                        Mathematics and its Applications, a Fellow of the British Computer Society, a 
                        Chartered Mathematician, Chartered Engineer, a past president of the Complex 
                        Systems Society and a Board Member of the UNESCO UniTwin Digital Campus for 
                        Complex Systems. He is the author of a number of books with his latest being 
                        “Hypernetworks in the science of complex systems”
                </div>
            </div>

            <div class="keynote margin-bottom-larger">
                <div class="image">
                    <img src="images/keynote_wolfram.png" width="150"
                     height="150" border="0" class="bordered" />
                </div>
                <div class="text">
                    <h2>Stephen Wolfram,<br /> Wolfram Research</h2>

                    Stephen Wolfram is a world leader in the field of Scientific Computing. He 
                    famously completed his PhD in Theoretical Physics from Caltech at the age of 20 
                    and is the youngest ever recipient of a MacArthur Fellowship at the age of 22. 
                    Following his PhD he went on to study at the renowned Institute for Advanced 
                    Study in Princeton and as a Professor of Physics, Mathematics and Computer 
                    Science at the University of Illinois. During this period, Stephen made ground 
                    breaking contributions to the field of Complex Systems. In particular his work 
                    on simple one-dimensional Cellular Automata is seen as a fundamental 
                    contribution to understanding the origin of Complexity. In 1986 he founded the 
                    first research centre and first journal in Complex Systems. Based on his 
                    research, Stephen published the widely acclaimed best-selling book ‘A New Kind 
                    of Science’ in 2002. Aside from Academia, Stephen founded the company Wolfram 
                    Research in 1987. Wolfram Research has developed the hugely successful 
                    technical computing software - Mathematica, the knowledge based search engine 
                    Wolfram|Alpha and in 2014 the release of Wolfram Language.
                        
                </div>
            </div>
<!--            
            <div class="keynote margin-bottom-larger">
                <div class="image"><img src="images/keynote_newman.jpg" width="150" height="198" border="0" class="bordered" /></div>
                <div class="text">
                    <h2>Professor Mark Newman,<br />University of Michigan, US</h2>
                    Professor Mark Newman is Paul Dirac Collegiate Professor of Physics and
professor at the Center for the Study of Complex Systems at the
University of Michigan as well as an external faculty member of the
Santa Fe Institute. He is probably best known for his seminal
work on complex networks and has a particular interest in social
networks and graph theory. Newman's research group has been highly
influential in the development of measures and techniques to quantify
such networks, in particular producing algorithms to better detect
community structure.<br /><br />

In recent years Newman has additionally gained substantial media
attention for the development of a new method to generate area
cartograms, resulting in the popular book "The Atlas of the Real World"
as well widely used visualisations of the 2012 US presidential election
results. The latest of his seven published books is called "Networks: An
Introduction" and provides a comprehensive guide to the theory of the
science of networks, which is rapidly establishing itself as the
standard textbook in the area.
                <br /><br />
                </div>
           </div>
           
            <div class="keynote">
                <div class="image"><img src="images/keynote_szathmary.jpg" width="150" height="198" border="0" class="bordered" /></div>
                <div class="text">
                    <h2>Professor E&ouml;rs Szathm&aacute;ry,<br />E&ouml;tv&ouml;s Lor&aacute;nd University, Budapest, Hungary</h2>
                    Professor E&ouml;rs Szathm&aacute;ry is a theoretical evolutionary biologist, whose work focuses on major transitions in evolution such as the origin of life, the rise of the first cellular organisms, and the development of human language. Szathm&aacute;ry’s best known work is on the origins of the genetic code, an analysis of epistasis in terms of metabolic control theory as well as a derivation of the optimal size of the genetic alphabet. Szathm&aacute;ry has also provided a general framework for discussing the transitions in evolution and particularly the growth of complexity in the living world. He attributes this increase in complexity to shifts in information storage and transmission, which, in turn, result in a series of major evolutionary transitions. This research is summarized in his two seminal books co-authored with a fellow theoretical biologist John Maynard Smith: <em>"The Major Transitions in Evolution"</em> and <em>"The Origins of Life"</em>. 
                <br /><br />
                </div>
-->
           </div>

        </div>

        
<?php include("includes/bodyBottom.php"); ?>
   


</body>
</html>
