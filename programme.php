<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Programme";
	$pageDescription = "The Student Conference on Complexity Science programme";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>

<?php include("includes/bodyTop.php"); ?>
		<?php include("includes/pageTitle.php"); ?>
    	
        
        <?php include("includes/navigation.php"); ?>
       
        
        <div class="content">
            <!--
			<strong>Legend:</strong> FY = foyer, LT = lecture theatre<br />
            <a href="downloads/SCCS_programme_timetable.pdf" target="_blank">Download the programme as a PDF</a><br />
            <strong>Abstracts:</strong> Individual abstract booklets are linked in each session title. You can also download the <a href="downloads/Abstract_booklet_full.pdf" target="_blank">full book of abstracts</a>.<br /><br />
           -->
           
           <div class="programme"> Provisional Timetable </div>
            
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
                <td width="6%">&nbsp;</td>
                <td width="20%"><strong>Tuesday</strong></td>
                <td width="27%"><strong>Wednesday</strong></td>
                <td width="27%"><strong>Thursday</strong></td>
                <td width="20%"><strong>Friday</strong></td>
            </tr>
            <tr>
                <td>08:00</td>
                <td>&nbsp;</td>
                <td class="colRegistration">Registration </td>
                <td class="">&nbsp;</td>
                <td class="">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>09:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colKeynote noborder-bottom">Welcome and Keynote</td>
                <td class="noborder-bottom colKeynote">Keynote</td>
                <td class="colRegistration noborder-bottom">Alumni Panel</td>
            </tr>
            <tr>
                <td>09:30</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colKeynote noborder-top">&nbsp;</td>
                <td class="noborder-top colKeynote">&nbsp;</td>
                <td class="colRegistration noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>10:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colBreak">Poster + Coffee</td>
                <td class="colBreak">Poster + Coffee</td>
                <td class="colBreak">Poster + Coffee</td>
            </tr>
            <tr>
                <td>10:30</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colBreak noborder-top">&nbsp;</td>
                <td class="colBreak noborder-top">&nbsp;</td>
                <td class="colBreak noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>11:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colSession noborder-bottom">&nbsp;</td>
                <td class="colSession noborder-bottom">&nbsp;</td>
                <td class="colSession noborder-bottom">&nbsp;</td>
            </tr>
            <tr>
                <td>11:20</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colSession noborder-top noborder-bottom">Presentations</td>
                <td class="colSession noborder-top noborder-bottom">Presentations</td>
                <td class="colSession noborder-top noborder-bottom">Presentations</td>
            </tr>
            <tr>
                <td><strong>12:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td>12:30</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-bottom colBreak">&nbsp;</td>
                <td class="noborder-bottom colBreak">&nbsp;</td>
                <td class="noborder-bottom colBreak">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>13:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colBreak noborder-top noborder-bottom">Lunch</td>
                <td class="colBreak noborder-top noborder-bottom">Lunch</td>
                <td class="colBreak noborder-top noborder-bottom">Lunch</td>
            </tr>
            <tr>
                <td>13:30</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top colBreak">&nbsp;</td>
                <td class="noborder-top colBreak">&nbsp;</td>
                <td class="noborder-top colBreak">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>14:00</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colSession noborder-bottom">Presentations</td>
                <td class="colSession noborder-bottom">Presentations</td>
                <td class="colSession noborder-bottom">Presentations</td>
            </tr>
            <tr>
                <td>14:30</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>15:00</strong></td>
                <td class="noborder-bottom colRegistration">&nbsp;</td>
                <td class="colBreak">Coffee</td>
                <td class="colBreak">Coffee</td>
                <td class="colBreak">Coffee</td>
            </tr>
            <tr>
                <td><strong>15:30</strong></td>
                <td class="noborder-top noborder-bottom colRegistration">&nbsp;</td>
                <td class="colSession noborder-bottom">Presentations</td>
                <td class="colWorkshop noborder-bottom">Workshop</td>
                <td class="colSession noborder-bottom">Presentations</td>
            </tr>
            <tr>
                <td><strong>16:00</strong></td>
                <td class="noborder-top noborder-bottom colRegistration">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
                <td class="colWorkshop noborder-top">&nbsp;</td>
                <td class="colSession noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td>16:30</td>
                <td class="noborder-top noborder-bottom colRegistration">Registration</td>
                <td class="colBreak">Coffee</td>
                <td class="colBreak">Coffee</td>
                <td class="colBreak">Coffee</td>
            </tr>
            <tr>
                <td><strong>17:00</strong></td>
                <td class="noborder-top noborder-bottom colRegistration">&nbsp;</td>
                <td class="noborder-bottom colKeynote">Keynote</td>
                <td class="colWorkshop noborder-bottom">Poster</td>
                <td class="noborder-bottom colKeynote">Keynote</td>
            </tr>
            <tr>
                <td>17:30</td>
                <td class="noborder-top colRegistration">&nbsp;</td>
                <td class="colKeynote noborder-top">&nbsp;</td>
                <td class="colWorkshop noborder-top">&nbsp;</td>
                <td class="colKeynote noborder-top">&nbsp;</td>
            </tr>
<!--
            <tr>
                <td><strong>18:00</strong></td>
                <td class="colRegistration">Welcome [Jubilee FY]</td>
                <td class="colBreak noborder-top">[Jubilee FY]</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td>18:30</td>
                <td class="colKeynote noborder-bottom">Keynote: M. Newman</td>
                <td class="colBreak noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
            </tr>
            <tr>
                <td><strong>19:00</strong></td>
                <td class="colKeynote noborder-top">[Jubilee LT]</td>
                <td class="">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
            </tr>
             <tr>
                <td>19:30</td>
                <td class="colBreak noborder-bottom">Wine + BBQ</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
            </tr>
        -->
            <tr>
                <td><strong>&nbsp;</strong></td>
                <td class="">&nbsp;</td>
                <td class="">&nbsp;</td>
                <td class="">&nbsp;</td>
                <td class="">&nbsp;</td>
            </tr>

            <tr>
                <td><strong>TBC</strong></td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="noborder-top">&nbsp;</td>
                <td class="colBreak">Conference Dinner</td>
            </tr>

            </table>
            <!--
            <a name="workshops1"></a>
            <h1>Workshops Session 1: Tuesday, 13:30 - 15:30</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr> 
                <td width="33%" valign="top" class="colFultonLTA"><strong class="text-large"><a href="workshops.php#complexity">Complexity, Evolution and <br />the Origin Of Life</a></strong><br /><br />S. Stepney &amp; S. Hickinbotham<br />Room: <span class="colored">Fulton LTA</span></td>
                <td width="33%" valign="top" class="colFultonSem"><strong class="text-large"><a href="workshops.php#fdm">Finite Difference Modelling</a></strong><br /><br />M. Sonnewald &amp; M. Wood<br />Room: <span class="colored">Fulton seminar room 104</span></td>
                <td width="34%" valign="top" class="colFultonLTB"><strong class="text-large"><a href="workshops.php#software">Software Project Planning &amp; Management</a></strong><br /><br />L. Pitonakova &amp; I. Bush<br />Room: <span class="colored">Fulton LTB</span></td>
           	</tr>
            </table>
            
            <a name="workshops2"></a>
            <h1>Workshops Session 2: Tuesday, 16:00 - 18:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr> 
                <td width="33%" valign="top" class="colFultonLTA"><strong class="text-large"><a href="workshops.php#validation">The Power of Model Validation</a></strong><br /><br />M. Sonnewald &amp; M. Wood<br />Room: <span class="colored">Fulton LTA</span></td>
                <td width="33%" valign="top" class="colFultonSem"><strong class="text-large"><a href="workshops.php#productivity">Tools From a Productivity Toolbox</a></strong><br /><br />M. Albert<br />Room: <span class="colored">Fulton seminar room 104</span></td>
                <td width="34%" valign="top">&nbsp;</td>
           	</tr>
            </table>
            
            
            
            <a name="presentations1"></a>
            <h1>Presentations Session 1: Wednesday, 10:00 - 11:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
            	<td width="6%">&nbsp;</td>
                <td width="23%" class="colJubileeLT"><strong class="text-large">Simulation Techniques</strong><br /><span class="colored">Jubilee LT</span> | <a href="downloads/Abstract_booklet_engin.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colJubileeLT144"><strong class="text-large">Language and Social Dynamics</strong><br /><span class="colored">Jubilee LT144</span> | <a href="downloads/Abstract_booklet_social.pdf" target="_blank">Abstracts</a></td>
                <td width="23%" class="colFultonLTA"><strong class="text-large">Earth Systems Complexity 1</strong><br /><span class="colored">Fulton LTA</span> | <a href="downloads/Abstract_booklet_earth.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colFultonLTB">&nbsp;</td>
           	</tr>
            <tr>
            	<td valign="top"><strong>10:00</strong></td>
                <td valign="top" class="colTalk">Simulation Techniques: The Finite Element Method<br /><span class="author">R. Carey</span></td>
                <td valign="top" class="colTalk">Why not to learn from each other - A new approach to investigate the evolution of social learning<br /><span class="author">M. Smolla</span></td>
                <td valign="top" class="colTalk">Resilience of the Amazon rainforest under human<br />impact<br /><span class="author">B. Wuyts</span></td>
                <td valign="top" class="colTalk"></td>
            </tr>
            <tr>
            	<td valign="top"><strong>10:20</strong></td>
                <td valign="top" class="colTalk">Simulation Techniques: Markov-Chain Monte Carlo Methods<br /><span class="author">M. Spraggs</span></td>
                <td valign="top" class="colTalk">Measuring Success in Communication<br /><span class="author">M. McGovern</span></td>
                <td valign="top" class="colTalk">Investigating Cenozoic climate change and carbon cycle conundrums using simple numerical models and analysis<br /><span class="author">A. Armstrong McKay</span></td>
                <td valign="top" class="colTalk"></td>
            </tr>
            <tr>
            	<td valign="top"><strong>10:40</strong></td>
                <td valign="top" class="colTalk">Simulation Techniques: From Atoms to Planets: “Molecular” Dynamics as a Simulation Tool<br /><span class="author">C. Cave-Ayland</span></td>
                <td valign="top" class="colTalk">Vocabulary growth curves: from random to real books<br /><span class="author">F. Font-Clos</span></td>
                <td valign="top" class="colTalk">Homeostasis in random ecosystems<br />impact<br /><span class="author">I. Weaver</span></td>
                <td valign="top" class="colTalk"></td>
            </tr>
            </table>
            
            <a name="presentations2"></a>
            <h1>Presentations Session 2: Wednesday, 11:20 - 13:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
            	<td width="6%">&nbsp;</td>
                <td width="23%" class="colJubileeLT"><strong class="text-large">Spatial Networks</strong><br /><span class="colored">Jubilee LT</span> | <a href="downloads/Abstract_booklet_networks.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colJubileeLT144"><strong class="text-large">Society and Technology</strong><br /><span class="colored">Jubilee LT144</span> | <a href="downloads/Abstract_booklet_social.pdf" target="_blank">Abstracts</a></td>
                <td width="23%" class="colFultonLTA"><strong class="text-large">Ecology</strong><br /><span class="colored">Fulton LTA</span> | <a href="downloads/Abstract_booklet_biol.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colFultonLTB"><strong class="text-large">Swarm Robotics</strong><br /><span class="colored">Fulton LTB</span> | <a href="downloads/Abstract_booklet_swarmRob.pdf" target="_blank">Abstracts</a></td>
           	</tr>
            <tr>
            	<td valign="top"><strong>11:20</strong></td>
                <td valign="top" class="colTalk">Planar Growth of Spatial Networks<br /><span class="author">G. Haslett</span></td>
                <td valign="top" class="colTalk">In search of a model of human dynamics analysis applied to social sciences<br /><span class="author">D. Martins</span></td>
                <td valign="top" class="colTalk">The swarm and the mosquito: emergent group properties arising from local acoustic interactions<br /><span class="author">A. Aldersley</span></td>
                <td valign="top" class="colTalk">Understanding the role of recruitment in collective robot foraging<br /><span class="author">L. Pitonakova</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>11:40</strong></td>
                <td valign="top" class="colTalk">The impact of constrained rewiring on network structure and node dynamics<br /><span class="author">P. Rattana</span></td>
                <td valign="top" class="colTalk">Anatomy of Scientifc Evolution<br /><span class="author">J. Yun</span></td>
                <td valign="top" class="colTalk">Landscape-scale conservation: the role of space and time in the realisation of biodiversity benefits<br /><span class="author">N. Synes</span></td>
                <td valign="top" class="colTalk">Communication in a Swarm of Robots<br /><span class="author">R. Miletitch</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:00</strong></td>
                <td valign="top" class="colTalk">The effects of social fallout on friendship circles<br /><span class="author">E. zu Erbach-Schoenberg</span></td>
                <td valign="top" class="colTalk">Understanding infrastructure, shared resources at all biological levels<br /><span class="author">R. Thanki</span></td>
                <td valign="top" class="colTalk">Citizen participation in ecological monitoring: the story of the New Forest cicada<br /><span class="author">D. Zilli</span></td>
                <td valign="top" class="colTalk">Enhance The Exogenous Fault Detection Approach by Analysing Transferable Data in Swarm Robotics<br /><span class="author">A. Khadidos</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:20</strong></td>
                <td valign="top" class="colTalk">Soft Random Geometric Graphs: Obstructions and Non-Convexity<br /><span class="author">A. Giles</span></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">Big Fish and Wiggly Lines: Using Hidden Markov Models to Describe Imperfect Time-series<br /><span class="author">J. Scutt Phillips</span></td>
                <td valign="top" class="colTalk">Hardware variation in mobile swarm robots<br /><span class="author">B. Shang</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:40</strong></td>
                <td valign="top" class="colTalk">Null Models for Community Detection in Spatially-Embedded, Temporal Networks<br /><span class="author">M. Sarzynska</span></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">&nbsp;</td>
            </tr>
            </table>
            
            <a name="presentations3"></a>
            <h1>Presentations Session 3: Wednesday, 14:00 - 16:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
            	<td width="6%">&nbsp;</td>
                <td width="23%" class="colJubileeLT"><strong class="text-large">Network Science Applications</strong><br /><span class="colored">Jubilee LT</span> | <a href="downloads/Abstract_booklet_networks.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colJubileeLT144"><strong class="text-large">Agent-based Modelling</strong><br /><span class="colored">Jubilee LT144</span> | <a href="downloads/Abstract_booklet_social.pdf" target="_blank">Abstracts</a></td>
                <td width="23%" class="colFultonLTA"><strong class="text-large">Earth Systems Complexity 2</strong><br /><span class="colored">Fulton LTA</span> | <a href="downloads/Abstract_booklet_earth.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colFultonLTB"><strong class="text-large">Evolution and the Origin of Life</strong><br /><span class="colored">Fulton LTB</span> | <a href="downloads/Abstract_booklet_evol.pdf" target="_blank">Abstracts</a></td>
           	</tr>
            <tr>
            	<td valign="top"><strong>14:00</strong></td>
                <td valign="top" class="colTalk">Analysing the Structure of Human Language via Phonological Networks<br /><span class="author">M. Stella</span></td>
                <td valign="top" class="colTalk">ABM in Digital Humanities: the case study of the Movius Line<br /><span class="author">I. Romanowska</span></td>
                <td valign="top" class="colTalk">Predicting global ocean circulation from north-south pressure gradients<br /><span class="author">E. Butler</span></td>
                <td valign="top" class="colTalk">Learning General Models from Past Environments<br /><span class="author">K. Kouvaris</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>14:20</strong></td>
                <td valign="top" class="colTalk">Multiple Time Scales and Hub Structure Observed in Spontaneously Evolved Neurons on High-density CMOS Electrode Array<br /><span class="author">E. Matsuda</span></td>
                <td valign="top" class="colTalk">Insights to past migration behaviour in the Maldives<br /><span class="author">L. Speelman</span></td>
                <td valign="top" class="colTalk">Western boundary current dynamics - are they really that simple?<br /><span class="author">E. Doddridge</span></td>
                <td valign="top" class="colTalk">The Evolution of Short-Term and Long-Term Evolvability in Gene-Regulation Networks<br /><span class="author">L. Kounios</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>14:40</strong></td>
                <td valign="top" class="colTalk">Brain signal complexity correlates with conscious level<br /><span class="author">M. Schartner</span></td>
                <td valign="top" class="colTalk">The use of participatory approaches and agent-based modelling to explore the complexity of food security within rural Malawi<br /><span class="author">S. Dobbie</span></td>
                <td valign="top" class="colTalk">Ekman's demon: Ocean-atmosphere communication in climate models<br /><span class="author">M. Sonnewald</span></td>
                <td valign="top" class="colTalk">The relationship between the adaptation of individual species and whole ecosystem behaviour<br /><span class="author">D. Power</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>15:00</strong></td>
                <td valign="top" class="colTalk">Community structure of a phone calls network<br /><span class="author">F. Botta</span></td>
                <td valign="top" class="colTalk">'But all my friends do it!' - evidence for a threshold model of social contagion<br /><span class="author">D. Sprague</span></td>
                <td valign="top" class="colTalk">Shallow-water gaseohydrothermal plume study after massive eruption at Panarea, Aeolian Islands, Italy<br /><span class="author">T. Tudino</span></td>
                <td valign="top" class="colTalk">Division of Labour games in relation to the Major Transitions in Evolution<br /><span class="author">S. Tudge</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>15:20</strong></td>
                <td valign="top" class="colTalk">Exploring the vulnerability of spatially complex infrastructure networks<br /><span class="author">C. Robson</span></td>
                <td valign="top" class="colTalk">Deciding to disclose: pregnancy and alcohol misuse<br /><span class="author">J. Gray</span></td>
                <td valign="top" class="colTalk">The Wavey Spin-up of an Antarctic Subpolar Gyre<br /><span class="author">C. Rye</span></td>
                <td valign="top" class="colTalk">Changing the Game<br /><span class="author">A. Jackson</span></td>
            </tr>
             <tr>
            	<td valign="top"><strong>15:40</strong></td>
                <td valign="top" class="colTalk">Modelling the formation and hierarchical network structures of covert illegal organisations assembled within law abiding social populations<br /><span class="author">D. Kerr</span></td>
                <td valign="top" class="colTalk">Gaussian Process Emulators for Bayesian Uncertainty and Sensitivity Analysis and Calibration of Agent Based Models<br /><span class="author">J. Hilton</span></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">Pre-template Metabolic Replicators: Genotype-Phenotype Decoupling as a Route to Evolvability<br /><span class="author">W. Hurndall</span></td>
            </tr>
            </table>
            
            <a name="presentations4"></a>
            <h1>Presentations Session 4: Thursday, 09:00 - 11:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
            	<td width="6%">&nbsp;</td>
                <td width="23%" class="colJubileeLT"><strong class="text-large">Dynamics on Complex Networks</strong><br /><span class="colored">Jubilee LT</span> | <a href="downloads/Abstract_booklet_networks.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colJubileeLT144"><strong class="text-large">Economics &amp; Finance </strong><br /><span class="colored">Jubilee LT144</span> | <a href="downloads/Abstract_booklet_econ.pdf" target="_blank">Abstracts</a></td>
                <td width="23%" class="colFultonLTA"><strong class="text-large">Darwinian Neurodynamics</strong><br /><span class="colored">Fulton LTA</span> | <a href="downloads/Abstract_booklet_darwinianNeurodynamics.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colFultonLTB"><strong class="text-large">Engineering and the Environment and Micromagnetics</strong><br /><span class="colored">Fulton LTB</span> | <a href="downloads/Abstract_booklet_engin.pdf" target="_blank">Abstracts</a></td>
           	</tr>
            <tr>
            	<td valign="top"><strong>09:00</strong></td>
                <td valign="top" class="colTalk">Robustness and self-organization in complex networks<br /><span class="author">X. Lu</span></td>
                <td valign="top" class="colTalk">Universality in Firm Dynamics: Insights from Big Data<br /><span class="author">A. Tang</span></td>
                <td valign="top" class="colTalk">Introduction to Darwinian Neurodynamics<br /><span class="author">E. Szathmary</span></td>
                <td valign="top" class="colTalk">Stability of power networks under higher penetration of renewable energy sources<br /><span class="author">L. Roberts</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>09:20</strong></td>
                <td valign="top" class="colTalk">Dynamical Systems Coupled in Heterogeneous Networks, Reduction and Synchronisation<br /><span class="author">M. Tanzi</span></td>
                <td valign="top" class="colTalk">An Agent-based Approach to Policy Making within the ICT System of Innovation<br /><span class="author">C. Hughes</span></td>
                <td valign="top" class="colTalk">Online Extreme Evolutionary Learning Machines<br /><span class="author">J. Auerbach</span></td>
                <td valign="top" class="colTalk">Exact coherent states in purely elastic parallel shear flows<br /><span class="author">T. Searle</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>09:40</strong></td>
                <td valign="top" class="colTalk">[Title TBC]<br /><span class="author">E. Barter</span></td>
                <td valign="top" class="colTalk">Multiple Agent Fashion Game<br /><span class="author">Z. Kosowska-Stamirowska</span></td>
                <td valign="top" class="colTalk">Guiding Search with Neural Networks<br /><span class="author">S. Sigtia</span></td>
                <td valign="top" class="colTalk">Optimal Strategies for Electricity Storage<br /><span class="author">E. Webborn</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>10:00</strong></td>
                <td valign="top" class="colTalk">Exploring Altruism in Social Networks<br /><span class="author">A. El-Bahrawy</span></td>
                <td valign="top" class="colTalk">An agent-based framework for analysing insolvency resolution mechanisms for banks<br /><span class="author">R. De Caux</span></td>
                <td valign="top" class="colTalk">Information transfer is not enough to preserve systematicity<br /><span class="author">E. Garcia-Casdemont</span></td>
                <td valign="top" class="colTalk">Micromagnetism and Skyrmions: The Complex Simulation Approach<br /><span class="author">M. Vousden</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>10:20</strong></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">The Emergence of Money: Network Formation in Monetary Search<br /><span class="author">T. Moran</span></td>
                <td valign="top" class="colTalk">Symbol representation in the brain: past and current hypotheses<br /><span class="author">Y. Knight</span></td>
                <td valign="top" class="colTalk">Finite size effects and stability of skyrmionic textures in nanostructures<br /><span class="author">M. Beg</span></td>
            </tr>
             <tr>
            	<td valign="top"><strong>10:40</strong></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">Good Vibrations: How to play the Magnetic Nano Flute<br /><span class="author">M. Albert</span></td>
            </tr>
            </table>
            
            <a name="presentations5"></a>
            <h1>Presentations Session 5: Thursday, 11:20 - 13:00</h1>
            <table cellpadding="0" cellspacing="0" border="0" class="bordered calendar" width="100%">
            <tr>
            	<td width="6%">&nbsp;</td>
                <td width="23%" class="colJubileeLT"><strong class="text-large">Structural Properties of Complex Networks</strong><br /><span class="colored">Jubilee LT</span> | <a href="downloads/Abstract_booklet_networks.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colJubileeLT144"><strong class="text-large">Planning &amp; Industry</strong><br /><span class="colored">Jubilee LT144</span> | <a href="downloads/Abstract_booklet_planning.pdf" target="_blank">Abstracts</a></td>
                <td width="23%" class="colFultonLTA"><strong class="text-large">Modelling Human Diseases</strong><br /><span class="colored">Fulton LTA</span> | <a href="downloads/Abstract_booklet_biol.pdf" target="_blank">Abstracts</a></td>
                <td width="24%" class="colFultonLTB"><strong class="text-large">Computational Chemistry</strong><br /><span class="colored">Fulton LTB</span> | <a href="downloads/Abstract_booklet_engin.pdf" target="_blank">Abstracts</a></td>
           	</tr>
            <tr>
            	<td valign="top"><strong>11:20</strong></td>
                <td valign="top" class="colTalk">The useage of nestedness for the analysis of bipartite networks<br /><span class="author">S. Beckett</span></td>
                <td valign="top" class="colTalk">Steering “Real World” Complex Adaptive Systems: Developing bio-based economy in the Humber region<br /><span class="author">A. Penn</span></td>
                <td valign="top" class="colTalk">How to Eliminate Solutes from the Brain: The Role of Arterial Pulsations<br /><span class="author">A. Diem</span></td>
                <td valign="top" class="colTalk">Time correlation function formalism: the case of computational spectroscopy<br /><span class="author">V. Vitale</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>11:40</strong></td>
                <td valign="top" class="colTalk">Random weighted networks with fixed strengths<br /><span class="author">F. Font-Clos</span></td>
                <td valign="top" class="colTalk">Exploring the Coevolution of Urban Form and Road Networks with Cellular Automata<br /><span class="author">O. Laslett</span></td>
                <td valign="top" class="colTalk">A Cellular Automaton Model of Atrial Fibrillation<br /><span class="author">K. Manani</span></td>
                <td valign="top" class="colTalk">Lipids - gel to liquid and back again, using meta dynamics to get a leg up in the free energy landscape<br /><span class="author">S. Wheeler</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:000</strong></td>
                <td valign="top" class="colTalk">[Title TBC]<br /><span class="author">M. Ritchie</span></td>
                <td valign="top" class="colTalk">ODD protocol application to review simulation model literature on complex social-technical systems within the metal recovery<br /><span class="author">S. R. Mueller</span></td>
                <td valign="top" class="colTalk">Modeling HIV virulence evolution in the context of immune escape<br /><span class="author">C. van Dorp</span></td>
                <td valign="top" class="colTalk">Recasting a model atomistic glassformer as an effective system of icosahedra<br /><span class="author">R. Pinney</span></td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:20</strong></td>
                <td valign="top" class="colTalk">The Entropy of Conditional Markov Trajectories Application to mobility predictability<br /><span class="author">M. Kafsi</span></td>
                <td valign="top" class="colTalk">Large Scale Queuing Network Based Pedestrian Evacuation Modelling Using Volunteered Geographical Information<br /><span class="author">B. Kunwar</span></td>
                <td valign="top" class="colTalk">Variational Bayesian analysis of blood glucose time series<br /><span class="author">Y. Zhang</span></td>
                <td valign="top" class="colTalk">&nbsp;</td>
            </tr>
            <tr>
            	<td valign="top"><strong>12:40</strong></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">Integrated Infrastructure Modelling - Managing Interdependencies with a Generic Approach<br /><span class="author">B. Dirks</span></td>
                <td valign="top" class="colTalk">&nbsp;</td>
                <td valign="top" class="colTalk">&nbsp;</td>
            </tr>
            </table>
            -->            
          
           
        </div>
    
        
<?php include("includes/bodyBottom.php"); ?>
   


</body>
</html>
