<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Research Themes";
	$pageDescription = "The SCCS 2014 conference will consist of 4 hands-on workshops and a number of parallel sessions of 3-6 papers. We particularly encourage sessions crossing traditional disciplinary boundaries or challenging the theoretical foundations of complexity science.";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>



<?php include("includes/bodyTop.php"); ?>
		<?php include("includes/pageTitle.php"); ?>
    	
        
        <?php include("includes/navigation.php"); ?>
<!--        <?php include("includes/ComingSoon.php"); ?> -->
        

        <div class="content">
            <strong>The conference will consist of several hands-on <a href="workshops.php">workshops</a> and a number of
parallel sessions of 3-6 presentations each. The general themes of the conference are as follows: </strong><br /><br />
            
            <div class="abstract-list">
            <ul>
            
                <li><h3>Theory of Complexity Science</h3></li>
                Self-organization, nonlinear dynamics and chaos, mathematical and simulation modeling methodology<br /><br />
                
                <li><h3>Network Science</h3></li>
                Technological networks, spatial networks, infrastructure, ecology, social networks, Internet<br /><br />

                <li><h3>Planning and Industry</h3></li>
                Critical infrastructures, urban planning, mobility, transport, sustainability<br /><br />

                <li><h3>Earth System Complexity</h3></li>
                Climate change, ocean, atmosphere, ice and solid earth dynamics<br /><br />

                <li><h3>Biological Complexity</h3></li>
                Darwinian neurodynamics, systems biology, ecology, ecosystem services, medicine<br /><br />
                
                <li><h3>Evolution and the Origin of Life</h3></li>
                Evolutionary systems, origin of life theory, major evolutionary transitions, generative and developmental systems, artificial life<br /><br />

                 <li><h3>Artificial Intelligence</h3></li>
                Swarm intelligence, embodied cognition, robotics, neuroscience<br /><br />

                 <li><h3>Social Systems</h3></li>
                Linguistics, demography, psychology, health, past societies<br /><br />

                 <li><h3>Economics and Finance</h3></li>
                Markets and stability, trade, public policy, game theory<br /><br />

                 <li><h3>Engineering and Physical Sciences</h3></li>
                Quantum dynamics, statistical mechanics, optimisation, turbulence, computational chemistry, nanotechnology, energy<br /><br />
          
            </ul>
        </div>
       
<?php include("includes/bodyBottom.php"); ?>
   

</body>
</html>
