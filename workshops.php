<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	//---- PAGE SETTINGS -------
	$pageTitle = "Workshops";
	//$pageDescription = "";
	//--------------------------
?>

<?php include ("includes/header.php"); ?>

<body>



<?php include("includes/bodyTop.php"); ?>
		<?php include("includes/pageTitle.php"); ?>
    	
        
        <?php include("includes/navigation.php"); ?>
        <!-- <?php include("includes/ComingSoon.php"); ?> -->
        
        
        <div class="content">
            
            <a name="complexity"></a>
            <div class="keynote margin-bottom-larger odd">
                <div class="text-full">
                    <h2> First steps in agent-based modelling with Netlogo </h2>
                    
                    <div class="image_thumb">
                        <img src="images/netlogo_iza.png" width="100%" />
                    </div>
                    
                    Agent-based modelling (ABM) has been taken by storm disciplines 
                    as diverse as ecology and social science. Now it is your turn to give it a go. 
                    Learn how to use the software in just under 2 hours and explore the 
                    possibilities that this popular complexity science technique may give you. We will 
                    provide an introduction to ABM using NetLogo, an open-source platform for 
                    building agent-based models, which combines an user-friendly interface, a simple 
                    coding language and a vast library of model examples, making it an ideal 
                    starting point for entry-level agent-based modellers, as well as a useful 
                    prototyping tool for more experienced programmers. The first part of the 
                    workshop will be devoted to demonstrating the basics of modelling with NetLogo 
                    through a set of worked examples. This should give each participant enough 
                    skill and confidence to tackle the second exercise: building a simple 
                    simulation in a small group. Finally, throughout the workshop we will hold a 
                    ‘drop-in’ clinic for anyone who would like to discuss their ideas for an 
                    agent-based simulation, needs help developing a model or would like to further 
                    explore the resources already developed by ABM modellers.

                    <br /><br />
                    
                    <strong>Run by:</strong> 
                    Iza Romanowska, a postgraduate research student at the Institute for Complex Systems
                    Simulation and the Centre for the Archaeology of Human Origins, University of Southampton, UK.
                    <br/><br/>
                    <strong>Intended audience:</strong> Anyone interested in the afore mentioned topic.
                    <br /><br />
                    <strong>Prerequisites:</strong> 
                     No prior knowledge of coding or ABM is required but we will ask the 
                    participants to bring their own laptops and install 
                    <a href url="https://ccl.northwestern.edu/netlogo/"> NetLogo </a> beforehand.
                    <br /><br />
        
                 </div>
            </div>
            
           
            
            <a name="fdm"></a>
            <div class="keynote margin-bottom-larger even">
            	<div class="text-full">
                    <h2> Software Carpentry </h2>
                    Details to be confirmed
                    <br /><br />
                    
                    <strong>Intended audience:</strong> Anyone interested in computational tools 
                    to improve and organize their research.<br /><br />
                    <strong>Prerequisites:</strong> 
                    <strong>Workshop materials:</strong><br />
                 </div>
            </div>
            
             <!--
            <a name="software"></a>
            <div class="keynote margin-bottom-larger odd">
            	<div class="text-full">
                    <h2>1C) Software Project Planning &amp; Management</h2>
                    The workshop will teach you how to plan and organise your software development work in order to achieve code reliability, readability and effectiveness. Agile development techniques, with focus on working within a remote team of people will also be introduced.<br /><br />
                   
                    <strong>Intended audience:</strong> Researchers who tend to work on long-term software projects and / or in collaboration with others<br /><br />
                    <strong>Prerequisites:</strong> None<br /><br />
        
                    <strong>Preliminary list of topics (subject to change):</strong><br />
                    The workshop will cover topics related to software engineering that are NOT about programming, but general things you do before you start writing code, including:
                    <ul>
                        <li>Agile development techniques and associated project planning in small teams</li>
                        <li>Object-oriented software design vs functional design</li>
                        <li>Most commonly used software development design patterns (finite-state-machines, model-view-controller, factory, etc.)</li>
                        <li>Planning your code - flow charts, class diagrams</li>
                        <li>Making your code easy to understand for other people (naming conventions, commenting, separating program into reusable functions, etc), sharing code</li>
                        <li>Introduction to managing your code in a remote repository</li>
                        <li>Management of feature creep</li>
                    </ul>
               </div>
            </div>
            
            <a name="validation"></a>
            <div class="keynote margin-bottom-larger even">
            	<div class="text-full">
                    <h2>2A) The Power of Model Validation</h2>
                    This workshop aims to arm participants with an overview of how even very general, basic theoretical models of natural or human processes can be validated against freely available data, with just a few adjustments for comparability! There are often unrecognised opportunities for models to be proven – or at least compared – with observational data that go unused simply because it is not obvious how to rephrase either the model or the data into comparable formats. This can be the case in fields like synthetic biology and climate modelling through to financial models. Validating with observational data allows for easier interpretation of a model's functioning and gives the research focus and a stronger impact.
                    <br /><br />
                    <strong>Intended audience:</strong> Anyone interested in real-world validation, possibly especially useful to ecologists & climatologists<br /><br />
                    <strong>Prerequisites:</strong> Please have python installed on your laptop.<br /><br />
        
                    <strong>Workshop materials:</strong><br />
                    <ul>
                        <li><a href="downloads/14_Brightontra.pdf" target="_blank">Workshop slides</a></li>
                        <li><a href="downloads/modelValidation_daisyWorld.pdf" target="_blank">Workshop slides 2</a></li>
                        <li><a href="downloads/GravityCurrent.zip" target="_blank">Code for the models used</a></li>   
                        <li><a href="downloads/SCCS2014_ModelValidation_spatialCorrelations.ipynb" target="_blank">iPython notebook</a></li>
                        <li><a href="downloads/modelValidationResources.pdf" target="_blank">Resource list</a></li>
                    </ul>
                </div>
            </div>
            
            
            <a name="productivity"></a>
            <div class="keynote odd">
            	<div class="text-full">
                    <h2>2B) Tools From a Productivity Toolbox - streamlining your workflow as a computational scientist</h2>
                    
                    Lost in your data or your simulation code? Frustrated by repetitive tasks or tools getting in your way? Need to quickly share & publish your computational results? Read on!<br /><br />
                    
                    Virtually all branches of science have profited from the vast increase in computational power in recent decades. However, given that scientists are rarely trained computer programmers (and more often than not self-taught), there is a substantial barrier to harnessing this power as it is easy to lose track of your work or get lost in the subsequent data analysis.<br /><br />
        
                    This workshop will provide a hands-on introduction to a variety of tools to make your workflow as a computational scientist more efficient, productive and reproducible.<br /><br />
         
                    This is *not* a workshop about programming. Rather, it is about the infrastructure and tools surrounding your daily simulation work. The aim is to help you build a solid framework for your computations so that you can focus on your research rather than wasting all your time trying to manage data and simulations.<br /><br />
                    
                    <strong>Intended audience:</strong> Any computational scientist who feels that parts of their workflow are unstructured or inefficient and is looking for ways to improve it.<br /><br />
                    
                    <strong>Prerequisites:</strong>
                    <ul>
                        <li>Basic programming experience will help but is not strictly required (Python is preferred, but the language mostly shouldn’t matter).</li>
                        <li>We will work quite a bit from the command line, so you should feel moderately comfortable in a terminal and know the basic commands for navigating between directories etc.</li>
                        <li>Participants are asked to install some necessary software on their laptops beforehand (detailed instructions will be sent around in advance).</li>
                    </ul>
                    <br />
                    <strong>Preliminary list of topics (subject to change):</strong><br />
                    <ul>
                        <li>The IPython Notebook: a versatile, interactive computing environment and electronic notebook (not just for Python!)</li>
                        <li>Basic version control with git</li>
                        <li>Makefiles for reproducible workflows and automation</li>
                        <li>Sumatra: a tool for keeping track of simulation & data anaysis runs (in particular for large parameter spaces), with the aim of supporting reproducible research</li>
                    </ul><br />
                    The focus will be on providing minimal working knowledge of these tools to enable participants to immediately start using them, rather than covering advanced features in great depth.   
                 </div>
             </div>
             -->
        </div>
        
<?php include("includes/bodyBottom.php"); ?>
   

</body>
</html>
